<?php
class LanguageLoader
{
    public function initialize()
    {
        $ci = &get_instance();
        $ci->load->helper('language');

        if (array_key_exists("HTTP_ACCEPT_LANGUAGE", $_SERVER)) {

            $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            
        } else {
            $lang = "english";
        }
        switch ($lang) {
            case "de":
                $ci->session->set_userdata('site_lang', "german");
                break;
            case "en":
                $ci->session->set_userdata('site_lang', "english");
                break;

        }
        $siteLang = $ci->session->userdata('site_lang');
        if ($siteLang) {

            $ci->lang->load('message', $siteLang);
        } else {

            $ci->lang->load('message', 'english');
            $ci->lang->load('message', 'english');
        }
    }
}
