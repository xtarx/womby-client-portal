<!-- saved from url=(0087)http://www.pdf.investintech.com/preview/cd9a2f68-3a15-11e6-879e-002590d31986/index.html -->
<html class="gr__pdf_investintech_com"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_003{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Times,serif;font-size:11.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Times,serif;font-size:11.1px;color:rgb(5,98,193);font-weight:normal;font-style:normal;text-decoration: underline}
div.cls_007{font-family:Times,serif;font-size:11.1px;color:rgb(5,98,193);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:Times,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Times,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Times,serif;font-size:8.1px;color:rgb(128,128,128);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_002{font-family:Times,serif;font-size:8.1px;color:rgb(128,128,128);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<body data-gr-c-s-loaded="true" cz-shortcut-listen="true">
<div style="position:absolute;left:50%;margin-left:-297px;top:0px;width:595px;height:841px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="<?php echo base_url('assets/') ?>/images/womby-sepa.jpg" width="595" height="841"></div>
<div style="position:absolute;left:70.80px;top:114.16px" class="cls_003"><span class="cls_003">Firma</span></div>
<div style="position:absolute;left:117.00px;top:114.40px" class="cls_003"><span class="cls_003" id="sepa_company_name"></span></div>
<div style="position:absolute;left:429.00px;top:117.76px" class="cls_003"><span class="cls_003">Wom.by GmbH</span></div>
<div style="position:absolute;left:429.00px;top:132.28px" class="cls_003"><span class="cls_003">Schleißheimerstr 6-10</span></div>
<div style="position:absolute;left:70.80px;top:140.80px" class="cls_003"><span class="cls_003">Straße</span></div>
<div style="position:absolute;left:117.00px;top:143.80px" class="cls_003"><span class="cls_003" id="sepa_street"></span></div>
<div style="position:absolute;left:429.00px;top:146.80px" class="cls_003"><span class="cls_003">80333 München</span></div>
<div style="position:absolute;left:70.80px;top:167.20px" class="cls_003"><span class="cls_003">Ort</span></div>
<div style="position:absolute;left:117.00px;top:170.80px" class="cls_003"><span class="cls_003" id="sepa_city"></span></div>
<div style="position:absolute;left:429.00px;top:169.24px" class="cls_003"><span class="cls_003">Tel: 089 / 21543898</span></div>
<div style="position:absolute;left:429.00px;top:183.76px" class="cls_003"><span class="cls_003">E-Mail: contact@wom.by</span></div>
<div style="position:absolute;left:429.00px;top:206.32px" class="cls_003"><span class="cls_003">Internet: </span><a href="http://www.wom.by/">www.wom.by</a> </div>
<div style="position:absolute;left:76.20px;top:239.44px" class="cls_005"><span class="cls_005">Unser Zeichen</span></div>
<div style="position:absolute;left:490.23px;top:239.44px" class="cls_005"><span class="cls_005">Datum</span></div>
<div style="position:absolute;left:490.23px;top:260.44px" class="cls_005"><span class="cls_005"><?php echo $date ?></span></div>
<div style="position:absolute;left:70.80px;top:291.52px" class="cls_003"><span class="cls_003">SEPA-Basislastschrift (Wiederkehrende Zahlungen)</span></div>
<div style="position:absolute;left:70.80px;top:313.48px" class="cls_003"><span class="cls_003">Ich ermächtige (Wir ermächtigen) die Wom.by GmbH, Schleißheimerstr. 6-10, 80333 München,</span></div>
<div style="position:absolute;left:70.80px;top:327.28px" class="cls_003"><span class="cls_003">Zahlungen von meinem (unserem) Konto mittels Lastschrift einzuziehen. Zugleich weise ich mein</span></div>
<div style="position:absolute;left:70.80px;top:341.08px" class="cls_003"><span class="cls_003">(weisen wir unser) Kreditinstitut an, die von der Wom.by GmbH auf mein (unser) Konto gezogenen</span></div>
<div style="position:absolute;left:70.80px;top:355.00px" class="cls_003"><span class="cls_003">Lastschriften einzulösen.</span></div>
<div style="position:absolute;left:70.80px;top:368.80px" class="cls_003"><span class="cls_003">Hinweis: Ich kann (Wir können) innerhalb von acht Wochen, beginnend mit dem Belastungsdatum,</span></div>
<div style="position:absolute;left:70.80px;top:382.60px" class="cls_003"><span class="cls_003">die Erstattung des belasteten Betrages verlangen.</span></div>
<div style="position:absolute;left:70.80px;top:410.32px" class="cls_003"><span class="cls_003">Gläubiger-Identifikationsnummer: DE15ZZZ00001904779</span></div>
<div style="position:absolute;left:71.16px;top:523.36px" class="cls_003"><span class="cls_003" id="sepa_company_name_2"></span></div>

<div style="position:absolute;left:70.80px;top:537.16px" class="cls_005"><span class="cls_005">Firmenname</span></div>
<div style="position:absolute;left:71.16px;top:571.00px" class="cls_003"><span class="cls_003" id="sepa_account_holder"></span></div>
<div style="position:absolute;left:70.80px;top:584.32px" class="cls_005"><span class="cls_005">Vor- und Nachname (Kontoinhaber)</span></div>
<div style="position:absolute;left:70.92px;top:621.04px" class="cls_003"><span class="cls_003" id="sepa_street_2"></span></div>
<div style="position:absolute;left:282.60px;top:621.04px" class="cls_003"><span class="cls_003" id="sepa_postal_city_country"></span></div>
<div style="position:absolute;left:70.80px;top:634.36px" class="cls_005"><span class="cls_005">Straße und Hausnummer</span></div>
<div style="position:absolute;left:474.10px;top:634.36px" class="cls_005"><span class="cls_005">PLZ und Ort</span></div>
<div style="position:absolute;left:70.92px;top:668.80px" class="cls_003"><span class="cls_003" id="sepa_bank_name" ></span></div>
<div style="position:absolute;left:281.88px;top:669.16px" class="cls_003"><span class="cls_003" id="sepa_iban"></span></div>
<div style="position:absolute;left:70.80px;top:683.44px" class="cls_005"><span class="cls_005">Kreditinstitut (Name)</span></div>
<div style="position:absolute;left:503.73px;top:683.44px" class="cls_005"><span class="cls_005">IBAN</span></div>
<div style="position:absolute;left:70.92px;top:720.04px" class="cls_003"><span class="cls_003" id="sepa_city_country" ><?php echo $date ?></span></div>
<div style="position:absolute;left:70.80px;top:731.68px" class="cls_005"><span class="cls_005">Datum, Ort und Unterschrift des Zahlungspflichtigen</span></div>
<div style="position:absolute;left:76.20px;top:760.72px" class="cls_002"><span class="cls_002">Wom.by GmbH</span></div>
<div style="position:absolute;left:189.48px;top:760.72px" class="cls_002"><span class="cls_002">Tel: 0176 / 9822 9881</span></div>
<div style="position:absolute;left:380.76px;top:760.72px" class="cls_002"><span class="cls_002">IBAN DE71 7015 0000 1004 1342 58</span></div>
<div style="position:absolute;left:76.20px;top:771.40px" class="cls_002"><span class="cls_002">Schleißheimerstr. 6-10</span></div>
<div style="position:absolute;left:189.48px;top:771.40px" class="cls_002"><span class="cls_002">E-Mail: social@wom.by</span></div>
<div style="position:absolute;left:288.60px;top:771.40px" class="cls_002"><span class="cls_002">Amtsgericht München</span></div>
<div style="position:absolute;left:380.76px;top:771.40px" class="cls_002"><span class="cls_002">BIC SSKMDEMMXXX</span></div>
<div style="position:absolute;left:76.20px;top:781.96px" class="cls_002"><span class="cls_002">80333 München</span></div>
<div style="position:absolute;left:189.48px;top:781.96px" class="cls_002"><span class="cls_002"> </span><a href="http://www.wom.by/">www.wom.by</a> </div>
<div style="position:absolute;left:288.60px;top:781.96px" class="cls_002"><span class="cls_002">HRB 224968</span></div>
</div>



<img id="hzDownscaled" style="position: absolute; top: -10000px;"></body></html>
