<?php $this->load->view('header');?>
<style type="text/css">
  .wom-thumb{
     height: 394px;
}
.wom-tile-guide .caption{
  padding-top: 70px;
}
.wom-tile-sepa .caption{
  padding-top: 35px;
}

</style>
<div class="container">
  <div class="errors">
    <?php echo validation_errors();
?>
  </div>


  <?php if ($this->session->flashdata('warnings')): ?>
  <div class="alert alert-warning" role="alert"><?php echo $this->session->flashdata('warnings');
?></div>
<?php endif;?>
  <!-- Row start -->
  <!-- Domain Info  -->
  <div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title">Shopware</h3>
        </div>
        <div class="panel-body">

          <!-- tiles start -->
          <div class="row wom-prods">

            <!-- woocommerce tile start -->
            <div class="col-sm-3 ">
              <div class="thumbnail wom-thumb">
                <a href="#" class="">
                  <img src="<?php echo base_url('assets/images/') ?>/Logo_Shopware_Professional.png" alt="...">
                </a>
                <div class="caption" >
                  <h3><?=$this->lang->line("dashboard_shopware_install")?></h3>
                  <p><a target="_blank"  href="<?php echo base_url('assets/plugins/shopware/Frontend.zip') ?>" class="btn btn-primary wom-dwn-btn " role="button">DOWNLOAD</a>
                </div>
              </div>
            </div>
            <!-- woocommerce tile end -->

  <!-- charity tile start -->
    <div class="col-sm-3 ">
              <div class="thumbnail wom-thumb">
                <a href="#" class="">
                  <img src="<?php echo base_url('assets/images/') ?>/logo-version-community-300x300.png" alt="...">
                </a>
                <div class="caption" >
                  <h3><?=$this->lang->line("dashboard_shopware_charity")?></h3>
                  <p>
  <?php if (has_filled_charity()): ?>
                  <a target="_blank"  href="<?php echo base_url('assets/plugins/shopware/charity/Frontend.zip') ?>" class="btn btn-primary wom-dwn-btn " role="button">DOWNLOAD</a>
<?php else: ?>

                  <a target="_blank"  href="<?php echo base_url('dashboard/charity/edit') ?>" class="btn btn-primary wom-dwn-btn " role="button"><?=$this->lang->line("dashboard_shopware_charity_fill_charity")?></a>

<?php endif;?>

                </div>
              </div>
            </div>
            <!-- charity tile end -->

            <!-- Tutorials tile start -->
            <div class="col-sm-3 wom-tile-guide">
              <div class="thumbnail wom-thumb">
                <a href="#" class="">
                  <img src="<?php echo base_url('assets/images/') ?>/installation-guide-icon.png" alt="...">
                </a>
                <div class="caption">
                  <h3><?=$this->lang->line("dashboard_install_guide")?></h3>
                  <p><a target="_blank" href="<?php echo base_url('assets/pdfs/shopware5.pdf') ?>" class="btn btn-primary wom-dwn-btn  " role="button">VIEW</a>
                </div>
              </div>
            </div>
            <!-- Tutorials tile end -->

         <!-- Facebook tile start -->
            <div class="col-sm-3  ">
              <div class="thumbnail wom-thumb">
                <a href="#" class="">
                  <img src="<?php echo base_url('assets/images/') ?>/F_icon.png" alt=Facebook">
                </a>
                <div class="caption">
                  <h3><?=$this->lang->line("dashboard_facebook")?></h3>
                  <p><a target="_blank" href="http://wom.by/installation-fb-id/" class="btn btn-primary wom-dwn-btn  " role="button">VIEW</a>
                </div>
              </div>
            </div>
            <!-- Facebook tile end -->


            <!-- PDF tile start -->
            <div class="col-sm-3 wom-tile-sepa ">
              <div class="thumbnail wom-thumb">
                <a href="#" class="">
                  <img src="<?php echo base_url('assets/images/') ?>/pdf_icon.png" alt="...">
                </a>
                <div class="caption">
                  <h3><?=$this->lang->line("dashboard_sepa")?></h3>
                  <p><a   href="<?php echo base_url('user/edit/') ?>" class="btn btn-primary wom-dwn-btn  " role="button">View</a>
                </div>
              </div>
            </div>
            <!-- PDF tile end -->


          </div>
          <!-- tiles end -->
        </div>
      </div>
    </div>
  </div>
  <!-- Domain Info  -->
  <!-- Row end -->

</div>
<?php $this->load->view('footer');?>