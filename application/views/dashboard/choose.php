<?php $this->load->view('header');?>
<style type="text/css">
  .wom-thumb{
     height: 394px;
}
.wom-tile-guide .caption{
  padding-top: 84px;
}
.wom-tile-sepa .caption{
  padding-top: 35px;
}

    .img-brd{
      border-box;
    border: 1px solid rgb(34, 57, 106);
    border-radius: 8px 8px 8px 8px;

      }




</style>
<div class="container">
  <div class="errors">
    <?php echo validation_errors();
?>
  </div>


  <?php if ($this->session->flashdata('warnings')): ?>
  <div class="alert alert-warning" role="alert"><?php echo $this->session->flashdata('warnings');
?></div>
<?php endif;?>
  <!-- Row start -->
 <!-- Domain Info  -->
<div class="row">
   <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
         <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>
            <h3 class="panel-title">Platform</h3>
         </div>
         <div class="panel-body">
            <div class="row vertical-top">
               <div class="col-md-4  text-default">
                  <p style="text-align: center; font-size: 22px; font-weight: 300;">WooCommerce</p>
                  <p style="text-align: center; font-size: 22px; font-weight: 300;"><span style="color: #333333;"><a href="<?echo base_url('dashboard/wooCommerce') ?>"><img class="img-brd selectsystembutton aligncenter wp-image-6153 size-full" src="<?php echo base_url('assets/images/') ?>/woocommerce_logo.png" alt="woo logo-01" width="150" height="150" sizes="(max-width: 150px) 100vw, 150px"></a></span></p>
                  <p style="text-align: center; font-size: 22px; font-weight: 300;"><?=$this->lang->line("dashboard_available")?></p>
               </div>

               <div class="col-md-4  text-default">
                  <p style="text-align: center; font-size: 22px; font-weight: 300;">Shopware</p>
                  <p style="text-align: center; font-size: 22px; font-weight: 300;"><span style="color: #333333;"><a href="<?echo base_url('dashboard/shopware') ?>"><img class="img-brd selectsystembuttonia aligncenter wp-image-6154 size-full hoverZoomLink" src="<?php echo base_url('assets/images/') ?>/shopware_logo.png"" alt="shopware" width="150" height="150"></a></span></p>
                  <p style="text-align: center; font-size: 22px; font-weight: 300;"><span style="color: #333333;"><?=$this->lang->line("dashboard_available")?></span></p>
               </div>
                <div class="col-md-4  text-default">
                  <p style="text-align: center; font-size: 22px; font-weight: 300;">Magento</p>
                  <p style="text-align: center; font-size: 22px; font-weight: 300;"><span style="color: #333333;"><a href="<?echo base_url('dashboard/magento') ?>"><img class="img-brd selectsystembuttonia aligncenter wp-image-6154 size-full" src="<?php echo base_url('assets/images/') ?>/magento-logo.png" alt="magento" width="150" height="150"  sizes="(max-width: 150px) 100vw, 150px"></a></span></p>
                  <p style="text-align: center; font-size: 22px; font-weight: 300;"><span style="color: #333333;"><?=$this->lang->line("dashboard_available")?></span></p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Domain Info  -->
  <!-- Row end -->

</div>
<?php $this->load->view('footer');?>