<?php $this->load->view('header');?>
<style type="text/css">
  .wom-thumb{
     height: 394px;
}
.wom-tile-guide .caption{
  padding-top: 84px;
}
.wom-tile-sepa .caption{
  padding-top: 35px;
}

.img-brd{
    border-box;
    border: 1px solid rgb(34, 57, 106);
    border-radius: 8px 8px 8px 8px;
    }
</style>
<div class="container">
  <div class="errors">
    <?php echo validation_errors();
?>
  </div>


  <?php if ($this->session->flashdata('warnings')): ?>
  <div class="alert alert-warning" role="alert"><?php echo $this->session->flashdata('warnings');
?></div>
<?php endif;?>
  <!-- Row start -->
 <!-- Domain Info  -->
<div class="row">


   <div class="col-md-12 col-sm-6 col-xs-12">
   

      <div class="panel panel-default">

         <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>
            <h3 class="panel-title">Platform</h3>
         </div>

         <div class="panel-body">
            <div class="row vertical-top">
    





              <!-- terms start -->
              <form id="registrationForm" class="form-horizontal">
  

    <!-- list selection start -->
    
        <div class="form-group">
              <div class="col-xs-8">
          <div class="col-xs-4">    

<input type="checkbox" id='Kabinet'>  <label for="check1">Kabinet</label>
<input type="checkbox" id='Monitor'>  <label for="check1">Monitor</label>
<input type="checkbox" id='Bravice'>  <label for="check1">Bravice</label>
</div>

    <div class="col-xs-4">
<input type="checkbox" id='Pojacalo'>  <label for="check1">Pojacalo</label>
<input type="checkbox" id='Coin usta'>  <label for="check1">Coin usta</label>
<input type="checkbox" id='Bar kod'>  <label for="check1">Bar kod</label>
</div>

<div class="col-xs-4">
<input type="checkbox" id='Toper'>  <label for="check1">Toper</label>
<input type="checkbox" id='Monitor + Touch'>  <label for="check1">Monitor+Touch</label>
<input type="checkbox" id='Brojaci'>  <label for="check1">Brojaci</label>   

    </div>

        </div>
        </div>

    <!-- list selection end -->





  <!-- start of terms 1 -->
    <div class="form-group">
        <label class="col-xs-3 control-label">Terms of use</label>
        <div class="col-xs-8">
            <div style="border: 1px solid #e5e5e5; height: 200px; overflow: auto; padding: 10px;">
Verkaufen Sie Produkte mit pornographischem Bezug, zum Beispiel in Form von 
Printmedien (Poster, Zeitschriften, Magazine etc.)
Prints auf Kleidung, Accessoires oder sonstigen Gegenständen
Filmen/Serien
Scherzartikel
Sexspielzeug


Verkaufen Sie eine der folgenden Produktgruppen?
Gebrauchswaffen
Jagdwaffen
Kriegswaffen
Nicht-tödliche Waffen
Sammlerwaffen
Sportwaffen
Dekorationswaffen
Schutzwaffen
Theaterwaffen


Verkaufen Sie Tabak, alkoholhaltige Produkte oder Produkte, die zum Konsum von Alkohol, Tabakwaren & Drogen auffordern?

Verkaufen Sie Produkte, die unter problematischen Arbeits- und Produktionsbedingungen hergestellt wurden? (Darunter fallen Kinderarbeit, Sklavenarbeit, fragwürdige Arbeitsbedingungen)

Verkaufen Sie Fleischwaren oder andere Tierprodukte aus Massentierhaltung oder Produkte die gegen das Tierschutzgesetz verstoßen?

Verkaufen Sie Produkte mit rassistischen, politisch radikalen, religiösen, sexistisch, homophoben oder sonstigen hetzerischen oder menschenverachtenden Inhalten in Form von
Printmedien (Poster, Zeitschriften, Magazine etc.)
Prints auf Kleidung, Accessoires, sonstigen Gegenständen Indizierte Filme, Videospiele oder Musik (= Musik, die auf der Liste der Bundesprüfstelle für jugendgefährdende Medien steht)
Filmen & Serien
Musik oder andere Audiodateien

            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="agree" value="agree" /> Agree with the terms and conditions
                </label>
            </div>
        </div>
    </div>

<!-- end of terms 1 -->



    <!-- submit button -->

    <div class="form-group">
        <div class="col-xs-9 col-xs-offset-3">
            <button type="submit" class="btn btn-primary" name="signup" value="Sign up">Submit</button>
        </div>
    </div>
</form>

              <!-- terms end -->
          
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Domain Info  -->
  <!-- Row end -->

</div>
<?php $this->load->view('footer');?>