<?php $this->load->view('header');?>
<style type="text/css">
  .wom-thumb{
     height: 394px;
}
.wom-tile-guide .caption{
  padding-top: 84px;
}
.wom-tile-sepa .caption{
  padding-top: 35px;
}

.img-brd{
    border-box;
    border: 1px solid rgb(34, 57, 106);
    border-radius: 8px 8px 8px 8px;
    }
    .check1{
          font-size: 1vw;
    }

    .blu-msg-1{
          width: 900px;
    }
    .blu-msg-2{
          width: 915px;
    }
</style>
<div class="container">
  <div class="errors">
    <?php echo validation_errors(); ?>
  </div>


  <?php if ($this->session->flashdata('warnings')): ?>
  <div class="alert alert-warning" role="alert"><?php echo $this->session->flashdata('warnings');
?></div>
<?php endif;?>
  <!-- Row start -->
 <!-- Domain Info  -->
<div class="row ">

   <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
         <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>
            <h3 class="panel-title">Charity</h3>
         </div>
         <div class="panel-body">

  <!-- top msg -->

        <h4 class="col-sm-offset-2 col-md-10 alert alert-info top-msg blu-msg-1">Pick the website</h4>
 <!-- top msg -->

            <div class="row vertical-top">



    <!-- list selection start -->

  <?php $selected_id = $this->uri->segment(3);?>

              <!-- terms start -->
              <form id="registrationForm" class="form-horizontal" method="post" action="<?=base_url('dashboard/do_charity/' . $selected_id)?>">


                <!-- website selection -->

  <div class="row">


  <div class="form-group">
         <label class="col-xs-3 control-label">Shop &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
        <div class="col-xs-8">


  <select class="form-control"  id="dynamic_select">
           <?php
foreach ($websites as $Website): ?>

    <option <?php if ($selected_id == $Website['id']) {echo "selected";}?> value="<?php echo $Website['id']; ?>/"><?php echo $Website['url']; ?></option>

 <?php endforeach;?>
  </select>

</div>
</div>
</div>
  <!-- website selection -->



</hr>
  <!-- start of terms 1 -->
    <!-- top msg -->

        <h4 class="col-sm-offset-2 col-md-10 alert alert-info top-msg blu-msg-2">Agree to disclaimer</h4>
 <!-- top msg -->

  <div class="row">
    <div class="form-group">
        <label class="col-xs-3 control-label">Terms of use</label>
        <div class="col-xs-8">
            <div style="border: 1px solid #e5e5e5; height: 250px; overflow: auto; padding: 10px;">
<!-- start terms-word -->
<p style="font-weight: bold;">Verkaufen Sie eine der folgenden Produktgruppen?</p>

<p>Gebrauchswaffen Jagdwaffen Kriegswaffen Nicht-t&ouml;dliche Waffen Sammlerwaffen Sportwaffen Dekorationswaffen Schutzwaffen Theaterwaffen   Verkaufen Sie Tabak, alkoholhaltige Produkte oder Produkte, die zum Konsum von Alkohol, Tabakwaren &amp; Drogen auffordern?</p>

<p>Verkaufen Sie Produkte, die unter problematischen Arbeits- und Produktionsbedingungen hergestellt wurden? (Darunter fallen Kinderarbeit, Sklavenarbeit, fragw&uuml;rdige Arbeitsbedingungen)</p>

<p>Verkaufen Sie Fleischwaren oder andere Tierprodukte aus Massentierhaltung oder Produkte die gegen das Tierschutzgesetz versto&szlig;en?</p>

<p>Verkaufen Sie Produkte mit rassistischen, politisch radikalen, religi&ouml;sen, sexistisch, homophoben oder sonstigen hetzerischen oder menschenverachtenden Inhalten in Form von</p>

<p> Printmedien (Poster, Zeitschriften, Magazine etc.) Prints auf Kleidung, Accessoires, sonstigen Gegenst&auml;nden Indizierte Filme, Videospiele oder Musik (= Musik, die auf der Liste der Bundespr&uuml;fstelle f&uuml;r jugendgef&auml;hrdende Medien steht) Filmen &amp; Serien Musik oder andere Audiodateien</p>
 <!-- end terms-word -->


            </div>
        </div>
    </div>
</div>
<!-- agree terms start -->

    <div class="form-group">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="checkbox">
                <label>
                    <input required type="checkbox"  name="confirmation_1" value="agree" /> Agree with the terms and conditions
                </label>
            </div>
        </div>
    </div>
    </div>
<!-- agree terms end -->
<!-- end of terms 1 -->


    <!-- submit button -->

<div class="row charity-agree">
<div class="col-sm-offset-2 col-md-8 text-center">
          <button type="submit" name="singlebutton" class="btn btn-large btn-primary ">Submit</button>
        </div>


</div>

</form>

              <!-- terms end -->

            </div>
         </div>
      </div>
   </div>

<!-- Domain Info  -->
  <!-- Row end -->

</div>




<?php $this->load->view('footer');?>

