<?php $this->load->view('header');?>

<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle stp-done">1</a>
        <p>Registration</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle stp-done" >2</a>
        <p>Billing</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Confirmation</p>
      </div>
      <?php if ($this->session->userdata('charity_signup')): ?>
     <div class="stepwizard-step">
        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
        <p>Charity</p>
      </div>
    <?php endif;?>

    </div>
  </div>




<div class="container cust-fields">
          <form class="form-horizontal row-border" method="post" action="<?=base_url('user/do_signup_billing')?>">

<?php if (validation_errors()): ?>
<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
<?php endif;?>
    <!-- Row start -->
    <!-- Billing Info  -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>
            <h3 class="panel-title"><?=$this->lang->line("signup_billing_data")?></h3>
          </div>

          <div class="panel-body">

            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_company_name")?></label>
                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="company_name" placeholder="<?=$this->lang->line("signup_company_name")?>" value="<?php echo set_value('company_name'); ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_account_name")?></label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="account_holder_name"  placeholder="<?=$this->lang->line("signup_account_name")?>" value="<?php echo set_value('account_holder_name'); ?>" >
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_street_name")?></label>
                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="street" placeholder="<?=$this->lang->line("signup_street_name")?> Name" value="<?php echo set_value('street'); ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_bank_name")?></label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="bank_name"  placeholder="Bank Name" value="<?php echo set_value('bank_name'); ?>">
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_postal_code")?></label>
                  <div class="col-xs-4">
                    <input required type="number" class="form-control" name="postal_code" placeholder="12345" value="<?php echo set_value('postal_code'); ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label">IBAN</label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="bank_iban"  placeholder="IBAN" value="<?php echo set_value('bank_iban'); ?>">
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_city")?></label>
                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="city" placeholder="<?=$this->lang->line("signup_city")?> Name" value="<?php echo set_value('city'); ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label">BIC</label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="bank_bic"  placeholder="BIC" value="<?php echo set_value('bank_bic'); ?>">
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_country")?></label>
                  <div class="col-xs-4">
                    <select name="country" class="form-control" id="country-name">
                      <?php foreach ($countries as $country): ?>
                      <option value="<?php echo $country['country_name'] ?>" <?php echo set_select('country', $country['country_name']); ?> ><?php echo $country['country_name'] ?></option>
                      <?php endforeach;?>
                    </select>
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_phone_number")?></label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="phone"  placeholder="eg. (555) 901-9012" value="<?php echo set_value('phone'); ?>">
                  </div>

                </div>
              </div>
            </div>
            <?php
$site_lang = $this->session->userdata('site_lang');

if ($site_lang && $site_lang == "english"): ?>
  <!-- VAT ONLY FOR NON-GERMAN -->
            </br>
            <div class="row">
              <div class="form-horizontal">

                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_vat")?></label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="vat"  placeholder="VAT" value="<?php echo set_value('vat'); ?>">
                  </div>

                </div>
              </div>
            </div>
            <!-- VAT ONLY FOR NON-GERMAN -->
          <?php endif;?>
            </br>

            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <div class="col-xs-6 col-xs-offset-4">
                    <div class="checkbox">
                      <label>
                        <input required type="checkbox" name="confirmation_1" value="agree" /> <?=$this->lang->line("signup_accept_1")?> <a target="_blank" href="http://wom.by/terms-of-use/"><?=$this->lang->line("signup_accept_terms")?>  </a> & <a target="_blank"  href="http://wom.by/kosten-empfehlungsmarketing-tools-wom-by/"><?=$this->lang->line("signup_accept_price")?></a> <?=$this->lang->line("signup_accept_2")?>
                      </label>
                    </div>

                  </div>

                </div>
              </div>
              </br>

            </div>

          </div>
        </div>
      </div>
      <!-- Billing Info  -->
      <!-- Row end -->
      <div class="row get-wom">

      <div class="row get-wom">

        <div class="col-md-12 text-center">
          <button type="submit" name="singlebutton" class="btn btn-large btn-primary "><?=$this->lang->line("signup_continue")?></button>
        </div>
      </div>
    </form>
  </div>
  <?php $this->load->view('footer');?>