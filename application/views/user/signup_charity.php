<?php $this->load->view('header');?>
  <?php $selected_id = $this->uri->segment(3);?>


<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle stp-done " >1</a>
        <p>Registration</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle stp-done" >2</a>
        <p>Billing</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle stp-done" >3</a>
        <p>Confirmation</p>
      </div>
       <div class="stepwizard-step">
        <a href="#step-4" type="button" class="btn btn-default btn-circle stp-done" disabled="disabled">4</a>
        <p>Charity</p>
      </div>

    </div>
  </div>

<div class="container cust-fields">
<?php if (validation_errors()): ?>
<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
<?php endif;?>

    <!-- Row start -->
    <!-- Personal Info  -->


    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>
            <h3 class="panel-title"><?=$this->lang->line("charity_terms_title")?></h3>
          </div>

          <div class="panel-body">

            <div class="row">
        <div class="form-group">
        </br>
        <label class="col-xs-3 control-label"><?=$this->lang->line("charity_terms_label")?></label>
        <div class="col-xs-8">
            <div style="border: 1px solid #e5e5e5; height: 250px; overflow: auto; padding: 10px;">
<!-- start terms-word -->
<b>Verkaufen Sie eine der folgenden Produktgruppen?</b>
</br>
</br>
<ul>
<li>
<p>Gebrauchswaffen, Jagdwaffen, Kriegswaffen, Nicht-t&ouml;dliche Waffen, Sammlerwaffen, Sportwaffen, Dekorationswaffen, Schutzwaffen, Theaterwaffen,   Verkaufen Sie Tabak, alkoholhaltige Produkte oder Produkte, die zum Konsum von Alkohol, Tabakwaren &amp; Drogen auffordern?</p>
</li>
<li>
<p>Verkaufen Sie Produkte, die unter problematischen Arbeits- und Produktionsbedingungen hergestellt wurden? (Darunter fallen Kinderarbeit, Sklavenarbeit, fragw&uuml;rdige Arbeitsbedingungen)</p>
</li>
<li>
<p>Verkaufen Sie Fleischwaren oder andere Tierprodukte aus Massentierhaltung oder Produkte die gegen das Tierschutzgesetz versto&szlig;en?</p>
</li>
<li>
<p>Verkaufen Sie Produkte mit rassistischen, politisch radikalen, religi&ouml;sen, sexistisch, homophoben oder sonstigen hetzerischen oder menschenverachtenden Inhalten in Form von Printmedien (Poster, Zeitschriften, Magazine etc.) Prints auf Kleidung, Accessoires, sonstigen Gegenst&auml;nden Indizierte Filme, Videospiele oder Musik (= Musik, die auf der Liste der Bundespr&uuml;fstelle f&uuml;r jugendgef&auml;hrdende Medien steht) Filmen &amp; Serien Musik oder andere Audiodateien</p>
</li>

 </ul>
 <!-- end terms-word -->


            </div>
        </div>
    </div>
            </div>
            </br>

  <!-- accept terms start -->
                <form class="form-horizontal row-border" method="post" action="<?=base_url('dashboard/do_charity')?>">
          <input type="hidden" name="charity_refrence" value="<?php echo $charity_refrence; ?>" />

      <div class="form-horizontal">

                <div class="">
                  <div class="col-xs-6 col-xs-offset-4">
                    <div class="checkbox">
                      <label>
                        <input required type="checkbox" name="confirmation_1" value="agree" />  <?=$this->lang->line("charity_terms")?>
                      </label>
                    </div>

                  </div>

                </div>
              </div>
  <!-- accept terms end -->
          </div>


        </div>
      </div>


    </div>
    <!-- Personal Info  -->
    <!-- Row end -->


  <!-- Row start -->
  <!-- select_charity start  -->
  <div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title"><?=$this->lang->line("signup_pick_charity")?></h3>
        </div>
        <div class="panel-body">
    <div class="col-sm-offset-2 col-md-8 col-sm-6 col-xs-12">

    <select name="charity" class="form-control" id="country-name">
                      <?php foreach ($charities as $charity): ?>
                      <option value="<?php echo $charity['id'] ?>" <?php echo set_select('charity', $charity['id']); ?> ><?php echo $charity['name'] ?></option>
                      <?php endforeach;?>
                    </select>


        </div>
        </div>
      </div>
    </div>
  </div>
  <!-- select_charity end  -->
  <!-- Row end -->


  <!-- Row start -->
  <!-- Domain Info  -->
  <div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title"><?=$this->lang->line("signup_sepa_title")?></h3>
        </div>
        <div class="panel-body">


            <div class="row-fluid">
              <div class="span8">
                <div class="mandat" id="mandatetext" style="overflow:scroll; border:1px solid #ccc; padding:10px; height:200px;">
                  <p><strong>SEPA-Lastschriftmandat(Wiederkehrende Zahlungen)</strong></p>


                  <p>Ich ermächtige (Wir ermächtigen) die Wom.by GmbH, Schleißheimerstr. 6-10, 80333 München, Zahlungen von meinem (unserem) Konto mittels Lastschrift einzuziehen. Zugleich weise ich mein (weisen wir unser) Kreditinstitut an, die von der Wom.by GmbH auf mein (unser) Konto gezogenen Lastschriften einzulösen.</p>
                  <p>Hinweis: Ich kann (Wir können) innerhalb von acht Wochen, beginnend mit dem Belastungsdatum, die Erstattung des belasteten Betrages verlangen.</p>
                  <dl>
                    <dt>Gläubiger-Identifikationsnummer:</dt>
                    <dd>DE48701500001004415293</dd>
                    <dt>BIC:</dt>
                    <dd>SSKMDEMMXXX</dd>
                    <dt>Mandatsreferenz:</dt>
                    <dd><?=$charity_refrence?></dd>
                    <dt>Firmenname:</dt>
                    <dd><?=$company_name?></dd>
                    <dt>Vor- und Nachname (Kontoinhaber):</dt>
                    <dd><?=$b_account_holder_name?></dd>
                    <dt>Straße und Hausnummer:</dt>
                    <dd><?=$street?></dd>
                    <dt>PLZ und Ort: </dt>
                    <dd><?=$postal_code?>, <?=$city?></dd>
                    <dt>Kreditinstitut (Name):</dt>
                    <dd><?=$b_name?></dd>
                    <dt>IBAN:</dt>
                    <dd><?=$b_IBAN?></dd>
                    <dt>Datum:</dt>
                    <dd><script type="text/javascript">var d=new Date()
                var tagesdatum=d.getDate();tagesdatum+="."+(d.getMonth()+1);tagesdatum+="."+d.getFullYear();document.write(tagesdatum)</script></dd>
                  </dl>
              </div>
            </div>
          </div>
          <!-- SEPA END -->
          <!-- checkbox start -->
          <div class="form-horizontal">
            <div class="">
              <div class="col-xs-6 col-xs-offset-4">
                <div class="checkbox">
                  <label>
                    <input required type="checkbox" name="confirmation_2" value="agree" /> <?=$this->lang->line("signup_sepa_agree")?>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <!-- checkbox end -->
           <!-- checkbox start -->
          <div class="form-horizontal">
            <div class="">
              <div class="col-xs-6 col-xs-offset-4">
                <div class="checkbox">
                  <label style="font-size: 12px;">
                    <input required type="checkbox" name="confirmation_3" value="agree"  /> Ich bestätige, dass für jeden Kalendermonat, in dem das Tool aktiviert ist, mindestens 5€ an Spenden an die von mir ausgewählten Organisation eingezogen werden dürfen. Die 5€ werden mit der Nutzung des Tools verrechnet. <span title="Pro Monat in dem das Tool in Ihrem Shop aktiviert ist, werden, sofern durch die Nutzung des Tools der Betrag von 5€ nicht erreicht wird, 5€ an die von Ihnen ausgewählte Organisation, ohne Abzüge, weitergeleitet. Die 5€ werden auf die Nutzung des Tools (generierte Spenden durch „Shares“) in Ihrem Shop verrechnet. Dies gewährleistet einen Sicherheit für die von Ihnen gewählten Hilfsorganisation, dass diese keine Spendenquittung für sehr geringe Beträge an Sie ausstellen muss. Ist das Tool in einem Abrechnungsmonat nicht in Ihrem Shop aktiviert entfällt auch der Mindestbetrag von 5€.">MEHR INFOS</span>


                  </label>
                </div>
              </div>
            </div>
          </div>
          <!-- checkbox end -->
        </div>
      </div>
    </div>
  </div>
  <!-- Domain Info  -->
  <!-- Row end -->


      <div class="row get-wom">

        <div class="col-md-12 text-center">
          <button type="submit" name="singlebutton" class="btn btn-large btn-primary "><?=$this->lang->line("signup_continue")?></button>
        </div>
      </div>
    </form>
  </div>
  <?php $this->load->view('footer');?>