<?php $this->load->view('header');?>

<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle stp-done">1</a>
        <p>Registration</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle stp-done" >2</a>
        <p>Billing</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Confirmation</p>
      </div>
      <?php if ($this->session->userdata('charity_signup')): ?>
     <div class="stepwizard-step">
        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
        <p>Charity</p>
      </div>
    <?php endif;?>

    </div>
  </div>




<div class="container cust-fields">
          <form class="form-horizontal row-border" method="post" action="<?=base_url('user/do_signup_billing')?>">

<?php if (validation_errors()): ?>
<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
<?php endif;?>
   //insert strip here
    STRIPE
      <div class="row get-wom">

      <div class="row get-wom">

        <div class="col-md-12 text-center">
          <button type="submit" name="singlebutton" class="btn btn-large btn-primary "><?=$this->lang->line("signup_continue")?></button>
        </div>
      </div>
    </form>
  </div>
  <?php $this->load->view('footer');?>