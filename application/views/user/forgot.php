<?php $this->load->view('header');?>


<div class="main-container">
<div class="container">
  <?php if (validation_errors()): ?>
  <div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
  <?php endif;?>

  <div class="alert alert-warning" role="alert">
    <?php if (isset($error)):
    echo $error;
endif;?>

           <?php echo $this->session->flashdata('success_msg'); ?>

  </div>
  <!-- login form start -->
  <!-- <div class="container"> -->
    <div class="row" id="pwd-container">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <section class="login-form">
          <form action="<?=base_url('user/do_forgot')?>" method="post" role="login">
            <h2>Reset My Password</h2>

            <input type="email" name="email" placeholder="Email" required class="form-control input-lg"  />
            <button type="submit"  class="btn btn-lg btn-primary btn-block wom-dwn-btn">Reset</button>
            <div>
             <a href="<?php echo base_url('user/login') ?>"><?=$this->lang->line("login_signin")?></a>
              oder
                      <a href="<?php echo base_url('user/signup') ?>"><?=$this->lang->line("login_create_account")?></a>
            </div>
          </form>
        </section>
      </div>
      <div class="col-md-4"></div>

  <!-- </div> -->
  <!-- login form end -->
</div>
</div>
</div>
<?php $this->load->view('footer');?>