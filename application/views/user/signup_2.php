<?php $this->load->view('header');?>


<?php if (!$edit): ?>
<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle stp-done">1</a>
        <p>Registration</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle stp-done" >2</a>
        <p>Billing</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle stp-done" >3</a>
        <p>Confirmation</p>
      </div>

            <?php if ($this->session->userdata('charity_signup')): ?>
     <div class="stepwizard-step">
        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
        <p>Charity</p>
      </div>
    <?php endif;?>

    </div>
  </div>

<?php endif;?>


<div class="container cust-fields">
  <div class="errors">
    <?php echo validation_errors(); ?>
  </div>
  <!-- Row start -->
  <!-- Domain Info  -->
  <div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title"><?=$this->lang->line("signup_sepa_title")?></h3>
        </div>
        <div class="panel-body">
          <form class="form-horizontal row-border" method="post" action="<?=base_url('user/do_confirm/') . "/" . $edit?>">

          <!-- SEPA Start -->
          <div class="row-fluid">
          <div class="span8">
                <div class="mandat" id="mandatetext" style="overflow:scroll; border:1px solid #ccc; padding:10px; height:200px;">

          <?php $site_lang = $this->session->userdata('site_lang');

if ($site_lang && $site_lang == "english") {
    $this->load->view('user/sepa/sepa_english', $this);
} else {
    $this->load->view('user/sepa/sepa_german', $this);

}

?>
</div>
 </div>
            </div>

          <!-- SEPA END -->
          <!-- checkbox start -->
          <div class="form-horizontal">
            <div class="">
              <div class="col-xs-6 col-xs-offset-4">
                <div class="checkbox">
                  <label>
                    <input required type="checkbox" name="confirmation_2" value="agree" /> <?=$this->lang->line("signup_sepa_agree")?>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <!-- checkbox end -->
        </div>
      </div>
    </div>
  </div>
  <!-- Domain Info  -->
  <!-- Row end -->
  <div class="row get-wom">
    <div class="col-md-12 text-center">
      <button type="submit" name="singlebutton" class="btn btn-large btn-primary ">
<?php if ($this->session->userdata('charity_signup')): ?>
<?=$this->lang->line("signup_continue")?>
<?php else: ?>
  GET WOM.by
<?php endif;?>


      </button>
    </div>
  </div>
</form>
</div>
<?php $this->load->view('footer');?>