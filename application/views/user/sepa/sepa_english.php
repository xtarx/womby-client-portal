<p class="p1">&nbsp;</p>
<table class="t1" style="height: 133px;" width="557" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="td1" valign="middle">
<p class="p3">Creditor:</p>
<p class="p3">Wom.by GmbH</p>
<p class="p3">Schlei&szlig;heimerstr. 6-10</p>
<p class="p3">80333 M&uuml;nchen</p>
<p class="p4">&nbsp;</p>
</td>
<td class="td1" valign="middle">
<p class="p3">Creditor identifier:</p>
<p class="p3">DE15ZZZ00001904779</p>
<p class="p4">&nbsp;</p>
<p class="p3">Mandate Reference:</p>
<p class="p5"><?=$token?></p>
</td>
</tr>
</tbody>
</table>
<p class="p3"><br />Type of Payment: Recurrent payment</p>
<p class="p3">By this mandate form, you authorize the creditor Wom.by GmbH, Schleissheimerstr. 6-10, 80333 Munich, to send instructions to your bank to debit your account and your bank to debit your account in accordance with the instructions from creditor Wom.by GmbH.</p>
<p class="p3">As part of your rights, you are entitled to a refund from your bank under the terms and conditions of your agreement with your bank. A refund must be claimed within 8 weeks starting from the date on which your account was debited.&nbsp;</p>
<p class="p3">Debtors bank</p>
<p class="p5"><?=$b_name?></p>
<p class="p3">Debtors SWIFT BIC</p>
<p class="p5"><?=$b_BIC?></p>
<p class="p3">Debtors IBAN</p>
<p class="p5"><?=$b_IBAN?></p>
<p class="p3">Debtor: (Name, address and land)</p>
<p class="p5"><?=$b_account_holder_name?>, <?=$street?> <?=$postal_code?> <?=$city?>,<?=$country?></p>
<p class="p6">&nbsp;</p>
<p class="p3">Date</p>
<p class="p5"><script type="text/javascript">var d=new Date()
                var tagesdatum=d.getDate();tagesdatum+="."+(d.getMonth()+1);tagesdatum+="."+d.getFullYear();document.write(tagesdatum)</script></p>
