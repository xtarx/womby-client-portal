
                  <p><strong>SEPA-Lastschriftmandat(Wiederkehrende Zahlungen)</strong></p>


                  <p>Ich ermächtige (Wir ermächtigen) die Wom.by GmbH, Schleißheimerstr. 6-10, 80333 München, Zahlungen von meinem (unserem) Konto mittels Lastschrift einzuziehen. Zugleich weise ich mein (weisen wir unser) Kreditinstitut an, die von der Wom.by GmbH auf mein (unser) Konto gezogenen Lastschriften einzulösen.</p>
                  <p>Hinweis: Ich kann (Wir können) innerhalb von acht Wochen, beginnend mit dem Belastungsdatum, die Erstattung des belasteten Betrages verlangen.</p>
                  <dl>
                    <dt>Gläubiger-Identifikationsnummer:</dt>
                    <dd>DE15ZZZ00001904779</dd>
                    <dt>Mandatsreferenz:</dt>
                    <dd><?=$token?></dd>
                    <dt>Firmenname:</dt>
                    <dd><?=$company_name?></dd>
                    <dt>Vor- und Nachname (Kontoinhaber):</dt>
                    <dd><?=$b_account_holder_name?></dd>
                    <dt>Straße und Hausnummer:</dt>
                    <dd><?=$street?></dd>
                    <dt>PLZ und Ort: </dt>
                    <dd><?=$postal_code?>, <?=$city?></dd>
                    <dt>Kreditinstitut (Name):</dt>
                    <dd><?=$b_name?></dd>
                    <dt>IBAN:</dt>
                    <dd><?=$b_IBAN?></dd>
                    <dt>Datum:</dt>
                    <dd><script type="text/javascript">var d=new Date()
                var tagesdatum=d.getDate();tagesdatum+="."+(d.getMonth()+1);tagesdatum+="."+d.getFullYear();document.write(tagesdatum)</script></dd>
                  </dl>
