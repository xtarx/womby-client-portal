<?php $this->load->view('header');

$is_charity = false;

if ($this->uri->segment(3) == "charity") {
    $is_charity = true;
}
?>



<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle stp-done">1</a>
        <p>Registration</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle " >2</a>
        <p>Billing</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Confirmation</p>
      </div>

<?php if ($is_charity): ?>
     <div class="stepwizard-step">
        <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
        <p>Charity</p>
      </div>
    <?php endif;?>
    </div>
  </div>

<div class="container cust-fields">
  <div class="alert alert-success text-center" role="alert"><b>
  <?php if ($is_charity): ?>

  <?=$this->lang->line("signup_charity_intro")?>

        <?php else: ?>
  <?=$this->lang->line("signup_intro")?>

        <?php endif;?>


  </b></div>

<?php if (validation_errors()): ?>
<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
<?php endif;?>

  <!-- Row start -->
  <!-- Domain Info  -->
  <div class="row">
<!--   <div class="alert alert-success text-center" role="alert"><b><?=$this->lang->line("signup_intro")?></b></div>
 -->

    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title"><?=$this->lang->line("signup_domain_name")?></h3>
        </div>

        <div class="panel-body">
        <div class="controls">
          <form class="form-horizontal row-border" method="post" action="<?=base_url('user/do_signup')?>">
            <div class="entry form-group">
              <label class="col-md-2 control-label"><?=$this->lang->line("signup_store_url")?></label>
              <div class="col-md-8">
                <input required type="url" name="company_url[]" onblur="checkURL(this)"  class="form-control" >
              </div>
                 <span class="input-group-btn">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
            </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- Domain Info  -->
    <!-- Row end -->
    <!-- Row start -->
    <!-- Personal Info  -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>
            <h3 class="panel-title"><?=$this->lang->line("signup_personal_data")?></h3>
          </div>

          <div class="panel-body">

            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label">Email</label>
                  <div class="col-xs-4">
                    <input required type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo set_value('email'); ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_password")?></label>

                  <div class="col-xs-4">
                    <input required type="password" class="form-control" name="password" id="password" >
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row ">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_confirm_email")?></label>
                  <div class="col-xs-4">
                    <input required type="email" class="form-control" name="confirm_email" id="confirm_email" placeholder='<?=$this->lang->line("signup_confirm_email")?>'>
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_confirm_password")?></label>

                  <div class="col-xs-4">
                    <input required type="password" class="form-control" name="confirm_password" id="confirm_password" >
                  </div>

                </div>
              </div>
            </div>

              </br>
            <div class="row ">
              <div class="form-horizontal">
            
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_country")?></label>
                  <div class="col-xs-4">
                    <select name="country" class="form-control" id="country-name">
                      <?php foreach ($countries as $country): ?>
                      <option value="<?php echo $country['country_name'] ?>" <?php echo set_select('country', $country['country_name']); ?> ><?php echo $country['country_name'] ?></option>
                      <?php endforeach;?>
                    </select>
                  </div>

                </div>
                
            </div>


          </div>

        </div>
      </div>
    </div>
    <!-- Personal Info  -->
    <!-- Row end -->

      <div class="row get-wom">

        <div class="col-md-12 text-center">
          <button type="submit" name="singlebutton" class="btn btn-large btn-primary "><?=$this->lang->line("signup_continue")?></button>
        </div>
      </div>
    </form>
  </div>
  <?php $this->load->view('footer');?>