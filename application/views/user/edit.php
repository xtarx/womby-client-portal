<?php $this->load->view('header');?>



<div class="container cust-fields">
<?php if (validation_errors()): ?>
<div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
<?php endif;?>

  <!-- Row start -->
  <!-- Domain Info  -->
  <div class="row">
  <?php if ($show_warning): ?>
<div class="alert alert-warning" role="alert"><?=$this->lang->line("edit_payment_warning")?></div>
<?php endif;?>
    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title"></h3>
        </div>

        <div class="panel-body">
        <div class="controls">
          <form class="form-horizontal row-border" method="post" action="<?=base_url('user/do_edit')?>">
           <?php foreach ($websites as $Website): ?>
            <div class="entry form-group">
              <label class="col-md-2 control-label"><?=$this->lang->line("signup_store_url")?></label>
              <div class="col-md-8">
                <input required type="url" name="company_url[]" onblur="checkURL(this)"  class="form-control" value="<?php echo $Website['url']; ?>">
              </div>
                <span class="input-group-btn">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
            </div>
          <?php endforeach;?>

            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- Domain Info  -->
    <!-- Row end -->
    <!-- Row start -->
    <!-- Personal Info  -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>
            <h3 class="panel-title">Personal Data</h3>
          </div>

          <div class="panel-body">

            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label">Email</label>
                  <div class="col-xs-4">
                    <input required type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $email; ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_password")?></label>

                  <div class="col-xs-4">
                    <input required type="password" class="form-control" name="password" id="password" >
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row ">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_confirm_email")?></label>
                  <div class="col-xs-4">
                    <input required type="email" class="form-control" name="confirm_email" id="confirm_email" value="<?php echo $email; ?>"placeholder="<?=$this->lang->line("signup_confirm_email")?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_confirm_password")?></label>

                  <div class="col-xs-4">
                    <input required type="password" class="form-control" name="confirm_password" id="confirm_password" >
                  </div>

                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- Personal Info  -->
    <!-- Row end -->

<!-- if has chairty -->
<?php if (isset($charities)): ?>
  <!-- Row start -->
  <!-- select_charity start  -->
  <div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
      <div class="panel panel-default">
        <div class="panel-heading clearfix">
          <i class="icon-calendar"></i>
          <h3 class="panel-title"><?=$this->lang->line("signup_pick_charity")?></h3>
        </div>
        <div class="panel-body">
    <div class="col-sm-offset-2 col-md-8 col-sm-6 col-xs-12">

    <select name="charity" class="form-control" id="charity-name">
                      <?php foreach ($charities as $charity): ?>
                      <option value="<?php echo $charity['id'] ?>"
 <?php if ($charity['id'] == $selected_charity) {
    echo "selected";
}
?>
><?php echo $charity['name'] ?></option>
                      <?php endforeach;?>
                    </select>


        </div>
        </div>
      </div>
    </div>
  </div>
  <!-- select_charity end  -->
  <!-- Row end -->
<?php endif;?>
<!-- if has chairty -->
    <!-- Row start -->
    <!-- Billing Info  -->
    <div class="row">
      <div class="col-md-12 col-sm-6 col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading clearfix">
            <i class="icon-calendar"></i>
            <h3 class="panel-title"><?=$this->lang->line("signup_billing_data")?></h3>
          </div>

          <div class="panel-body">

            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_company_name")?></label>
                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="company_name" placeholder="<?=$this->lang->line('signup_company_name')?>" value="<?php echo $company_name; ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line('signup_account_name')?></label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="account_holder_name"  placeholder="<?=$this->lang->line('signup_account_name')?>" value="<?php echo $b_account_holder_name; ?>" >
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line('signup_street_name')?></label>
                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="street" placeholder="<?=$this->lang->line('signup_street_name')?>" value="<?php echo $street; ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line('signup_bank_name')?></label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="bank_name"  placeholder="<?=$this->lang->line('signup_bank_name')?>" value="<?php echo $b_name; ?>">
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line('signup_postal_code')?></label>
                  <div class="col-xs-4">
                    <input required type="number" class="form-control" name="postal_code" placeholder="12345" value="<?php echo $postal_code; ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label">IBAN</label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="bank_iban"  placeholder="IBAN" value="<?php echo $b_IBAN; ?>">
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label">City</label>
                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="city" placeholder="City Name" value="<?php echo $city; ?>">
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label">BIC</label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="bank_bic"  placeholder="BIC" value="<?php echo $b_BIC; ?>">
                  </div>

                </div>
              </div>
            </div>
            </br>
            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_country")?></label>
                  <div class="col-xs-4">
                    <select name="country" class="form-control" id="country-name">
                      <?php foreach ($countries as $country_item): ?>
                      <option  value="<?php echo $country_item['country_name'] ?>"
                       <?php if ($country_item['country_name'] == $country) {
    echo "selected";
}
?>><?php echo $country_item['country_name'] ?></option>
                      <?php endforeach;?>
                    </select>
                  </div>

                </div>
                <div class="">
                  <label class="col-xs-2 control-label"><?=$this->lang->line("signup_phone_number")?></label>

                  <div class="col-xs-4">
                    <input required type="text" class="form-control" name="phone"  placeholder="eg. (555) 901-9012" value="<?php echo $phone; ?>">
                  </div>

                </div>
              </div>
            </div>
            </br>

            <div class="row">
              <div class="form-horizontal">
                <div class="">
                  <div class="col-xs-6 col-xs-offset-4">
                    <div class="checkbox">
                      <label>
                       <input required type="checkbox" name="confirmation_1" value="agree" /> <?=$this->lang->line("signup_accept_1")?> <a target="_blank" href="http://wom.by/terms-of-use/"><?=$this->lang->line("signup_accept_terms")?>  </a> & <a target="_blank"  href="http://wom.by/kosten-empfehlungsmarketing-tools-wom-by/"><?=$this->lang->line("signup_accept_price")?></a> <?=$this->lang->line("signup_accept_2")?>
                      </label>
                    </div>

                  </div>

                </div>
              </div>
              </br>

            </div>

          </div>
        </div>



      </div>
      <!-- Billing Info  -->
      <!-- Row end -->

      <div class="row get-wom">

        <div class="col-md-12 text-center">
          <button type="submit" name="singlebutton" class="btn btn-large btn-primary ">Continue</button>
        </div>
      </div>
    </form>
  </div>
  <?php $this->load->view('footer');?>