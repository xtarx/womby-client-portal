<?php $this->load->view('/header');?>

<div class="main-container bckgroundDia2">
<div class="container">
  <?php if (validation_errors()): ?>
  <div class="alert alert-danger" role="alert"><?php echo validation_errors(); ?></div>
  <?php endif;?>
  <?php if (isset($error)): ?>

  <div class="alert alert-warning" role="alert"><?php echo $error; ?></div>
  <?php endif;?>
  <!-- login form start -->
  <!-- <div class="container"> -->
    <div class="row" id="pwd-container ">
      <div class="col-md-4"></div>
      <div class="col-md-4">
        <section class="login-form  ">

          <form action="<?=base_url('user/do_login')?>" method="post" role="login">
            <h2><?=$this->lang->line("login_header")?></h2>

            <input type="email" name="email" placeholder="Email" required class="form-control input-lg"  />
            <input type="password" class="form-control input-lg" id="password" placeholder="Password" name="password" required="" />
            <button type="submit"  class="btn btn-lg btn-primary btn-block wom-dwn-btn"><?=$this->lang->line("login_signin")?></button>
            <div>
              <a href="<?php echo base_url('user/signup') ?>"><?=$this->lang->line("login_create_account")?></a>
              oder <a href="<?php echo base_url('user/forgot_password') ?>"><?=$this->lang->line("login_forgot_password")?></a>
            </div>
          </form>
        </section>
      </div>
      <div class="col-md-4"></div>

  <!-- </div> -->
  <!-- login form end -->
</div>
</div>
</div>
<?php $this->load->view('/footer');?>