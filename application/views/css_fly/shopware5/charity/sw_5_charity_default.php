<?header('Content-Type: text/css');?>
#wom-container
{   position: relative;
    padding-top: 10px;
    padding-right: 10px;
    padding-bottom: 10px;
    height: inherit;
    margin: inherit;
    border-top: 1px solid #bdc7c8;
    /*   border-bottom: 2px solid #bdc3c7;*/
    margin-bottom: 10px;
}
.clear
{
    clear: both;
}
.wom-price
{
    float: left;
    font-size: 18px;
    white-space: normal !important;
    line-height: 1.25 !important;
    height: 42px;
    max-width: 90%;
}
.wom-description
 {
     width:90%;
     padding-left: 53px;
     padding-right: 0px;
 }
.wom-sharing-options
{
    float: right;
    position: absolute;
}
.wom-amount
{
    color: #27ae60;
}
.wom-brand
{
    position: absolute;
    left: 0;
    bottom: 0;
}
.wom-logo
{
   /* width: 70px;*/
}
.wom-after-share-text
{
    padding-bottom: 10px;
}
.wom-sharing-options iframe
{
    max-width: none;
}
.wom-brand-after-share
{
    position: absolute;
    margin-top: 10px;
    top: 0;
    right: 0;
}

.wom-shr-dsc{
    margin-top: -30px;
    float: left;
    font-size: 11px;
    position:absolute;
  /*  padding-left: 54px;*/

}
#wom-thanks {
    position: relative;
    padding-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
    height: inherit;
    margin: inherit;
    border-top: 1px solid #bdc7c8;
    /*   border-bottom: 2px solid #bdc3c7;*/ /*   margin-bottom: 15px;*/
}

.wom-shr-thanks-message {
    width:90%;
    padding-top: 6px;
    font-size: 15px;
    white-space: normal !important;
    line-height: 1.25 !important;
    margin-left:85px;
    margin-bottom: 5px;
}
.wom-shr-thanks-dsc{
    margin-top: -15px;
    float: left;
    font-size: 11px;
    padding-left:5px;
}
.wom-chrty-thnks-logo{

    position: absolute;
    width: 15%;
    float: left;
    position: absolute;
    top: 18px;
}

.wom-chrty-logo{
    right: 5px;
    position: absolute;
    max-width: 15%;
    float: right;
    top: 12px;
}

.wom-thanks-helper-msg{
    padding-left:10px;
    font-size: .755rem;
    margin-bottom: -20px;
}
