<?header('Content-Type: text/css');?>
#wom-container {
    position: relative;
    padding-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
    padding-bottom: 10px;
    height: inherit;
    margin: inherit;
    border-top: 1px solid #bdc7c8;
 /*   border-bottom: 2px solid #bdc3c7;*/
    margin-bottom: 10px;
}
.clear {
    clear: both;
}
.wom-price {
    float: left;
    padding-top: 5px;
    padding-left: 56px;
    font-size: 18px;
    white-space: normal !important;
    line-height: 1.25 !important;
}

.wom-sharing-options {
    float: right;
    position: absolute;
}
.wom-amount {
    color: #27ae60;
}
.wom-brand {
    position: absolute;
    left: 0;
    bottom: 0;
}
.wom-logo {
    width: 70px;
}
.wom-after-share-text {
    padding-bottom: 10px;
}
.wom-sharing-options iframe {
    max-width: none;
}

.wom-brand-after-share{
    position: absolute;
    margin-top: 10px;
    top: 0;
    right: 0;

}

.wom-shr-dsc{
    padding-left: 58px;
    margin-top: -19px;
    float: left;
    font-size: 11px;
}
