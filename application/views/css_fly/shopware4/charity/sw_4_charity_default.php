<?header('Content-Type: text/css');?>
#wom-container {
    position: relative;
    padding-top: 10px;
    padding-bottom: 10px;
    height: inherit;
    margin: inherit;
    border-top: 1px solid #bdc3c7;
}
.clear {
    clear: both;
}
.wom-price {
    float: left;
    /*padding-top: 10px;*/
    font-weight: bold;
    font-size: x-large;
    margin-left: 40px !important;
    width: 70% !important;
    height: 40px;
}

.wom-sharing-options {
    float: right;
    position: absolute;
}
.wom-amount {
    color: #27ae60;
    padding-left: 5px;
}
.wom-brand {
    position: absolute;
    left: 0;
    bottom: 0;
}
.wom-logo
{
    width: 50px;
}
.wom-after-share-text
{
    padding-bottom: 10px;
}
.wom-sharing-options iframe
{
    max-width: none;
}
.wom-brand-after-share
{
    position: absolute;
    margin-top: 10px;
    top: 0;
    right: 0;
}
.wom-shr-dsc{
    margin-top: 5px;
    font-size:98% !important

}
#wom-thanks {
    position: relative;
    padding-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
    height: inherit;
    margin: inherit;
 /*   border-top: 1px solid #bdc7c8;*/
    /*   border-bottom: 2px solid #bdc3c7;*/ /*   margin-bottom: 15px;*/
}
.clear {
    clear: both;
    margin-top:20px;
}

.wom-shr-thanks-message {
    width:90%;
    padding-top: 12px;
    font-size: 15px;
    white-space: normal !important;
    line-height: 1.25 !important;
    margin-left:68px !important;
}
.wom-shr-thanks-dsc{
    margin-top: -15px;
    float: left;
    font-size: 11px;
    padding-left:5px;
}

.wom-chrty-logo{
    right: 0px;
    position: absolute;
    width: 15%;
    float: right;
    top: 12px;
}

.wom-chrty-thnks-logo{
    position: absolute;
    width: 15%;
    float: left;
    position: absolute;
    top: 18px;
}
