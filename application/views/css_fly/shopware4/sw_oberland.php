<?
header('Content-Type: text/css');
// print out your php-driven css...
?>

#wom-container {
    position: relative;
    padding-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
    height: inherit;
    margin: inherit;
    border-top: 1px solid #bdc7c8;
}
.clear {
    clear: both;
}
.wom-price {
    float: left;
    padding-top: 5px;
    padding-left:  45px !important;
    font-size: 15px;
    white-space: normal;
    line-height: 1.25;
}

.wom-sharing-options {
    float: right;
    position: absolute;
}
.wom-amount {
    color: #27ae60;
}
.wom-brand {
    position: absolute;
    left: 0;
    bottom: 0;
}
.wom-logo {
    width: 70px;
}
.wom-after-share-text {
    padding-bottom: 10px;
}
.wom-sharing-options iframe {
    max-width: none;
}

.wom-brand-after-share{
    position: absolute;
    margin-top: 10px;
    top: 0;
    right: 0;

}


.wom-shr-dsc{
    padding-left: 45px !important;
    margin-top: -3px !important;
    padding-top: 0px !important;
    float: left;
    font-size: 9px;
}
