<?
header('Content-Type: text/css');
// print out your php-driven css...
?>
#wom-container {
    position: relative;
    padding-top: 10px;
    padding-right: 10px;
}
.clear {
    clear: both;
}
.wom-price {
    float: left;
    padding-top: 9px;
    padding-left: 56px;
    font-size: 16px;
}

.wom-sharing-options {
    float: right;
    position: absolute;
    margin-top: 10px;
}
.wom-amount {
    color: #706f6f;
}
.wom-brand {
    position: absolute;
    left: 0;
    bottom: 0;
}
.wom-logo {
    width: 70px;
}
.wom-after-share-text {
    padding-bottom: 10px;
}
.wom-sharing-options iframe {
    max-width: none;
}

.wom-brand-after-share{
    position: absolute;
    margin-top: 10px;
    top: 0;
    right: 0;

}

.wom-shr-dsc{
    padding-left: 0px;
    margin-top: -14px;
    float: left;
    font-size: 10px;
}


@media screen and (min-width: 400px) and (max-width: 800px){
.wom-shr-dsc {
    padding-left: 58px;
    margin-top: -14px;
    float: left;
    font-size: 10px;
    position: relative;
}}




