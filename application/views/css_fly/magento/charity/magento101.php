<?header('Content-Type: text/css');
?>

#wom-container {
    position: relative;
    padding-top: 25px;
    padding-right: 0px;
    max-width: 34em !important;

    /*height: inherit;
    margin: inherit;
    */
}
.clear {
    clear: both;
}

.wom-sharing-options {
    float: left;
    position: absolute;
    margin-top: 0px;
   
}
.wom-price {
    float: left;
    padding-top: 0px;
    padding-left: 53px;
    text-overflow: ellipsis;
    height: 42px;
    word-wrap: initial;
    max-width: 61%;
    white-space: normal !important;
    line-height: 1.25 !important;
    font-size: 1.25em !important;
    margin-top: 3.5px;
    display:inline;
    
}

.wom-chrty-logo
{
    margin-left: 10px;
    max-width: 19%;
    float: left;
    right: 0px;
    margin-top: 10px;
    position: relative;
    }

.wom-amount {
    color: #27ae60;
}
.wom-brand {
    position: absolute;
    left: 0;
    bottom: 0;
}
.wom-logo {
    width: 70px;
}
.wom-after-share-text {
    padding-bottom: 10px;
}
.wom-sharing-options iframe {
    max-width: none;
}

.wom-brand-after-share{
    position: absolute;
    margin-top: 10px;
    top: 0;
    right: 0;

}

.wom-shr-dsc{

    margin-top: 0.1em;
    float: left;
    font-size: 9px;
    width: 100%;
    text-align: justify;
    color: #5f7285;
}




.wom-chrty-thnks-logo{
    position: absolute;
    width: 70px;
    float: left;
    position: absolute;
    top: 18px;
}
.wom-shr-thanks-dsc{
    position: relative;
    float: left;
    font-size: 11px;
    max-width:100%;
    padding-left:80px;
    color: #5f7285;
}
.wom-shr-thanks-message {
    width:90%;
    padding-top: 0px;
    font-size: 15px;
    white-space: normal !important;
    line-height: 1.25 !important;
    margin-left:80px !important;
    position: relative;
    float: left;
}
#wom-thanks {
    position: relative;
    padding-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
    height: inherit;
    margin: inherit;
    /*   border-top: 1px solid #bdc7c8;*/
    /*   border-bottom: 2px solid #bdc3c7;*/ /*   margin-bottom: 15px;*/
}

@media screen and (max-width: 330px) {
.wom-shr-dsc {
    margin-top: 1.5em;
    
}