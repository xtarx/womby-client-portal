<?header('Content-Type: text/css');
?>
#wom-container {
    position: relative;
    padding-top: 10px;
    padding-right: 10px;

    /*height: inherit;
    margin: inherit;
    */
}
.clear {
    clear: both;
}
.wom-price {
    float: left;
    padding-top: 3.5px;
    padding-left: 56px;
    text-overflow: ellipsis;
    height: 42px;
    word-wrap: initial;
    max-width: 190px;
    white-space: normal !important;
    line-height: 1.25 !important;
    font-size: 15px;
    margin-top: 6.5px;
}

.wom-sharing-options {
    float: right;
    position: absolute;
    margin-top: 10px;
}
.wom-amount {
    color: #27ae60;
}
.wom-brand {
    position: absolute;
    left: 0;
    bottom: 0;
}
.wom-logo {
    width: 70px;
}
.wom-after-share-text {
    padding-bottom: 10px;
}
.wom-sharing-options iframe {
    max-width: none;
}

.wom-brand-after-share{
    position: absolute;
    margin-top: 10px;
    top: 0;
    right: 0;

}
.wom-chrty-logo
{
    margin-left: 10px;
    position: absolute;
    width: 70px;
    float: right;
    top: 22px;
}
.wom-shr-dsc{

    margin-top: -2px;
    float: left;
    font-size: 9px;
    width: 100%;
    text-align: justify;
    color: #5f7285;
}

.wom-chrty-thnks-logo{
    position: absolute;
    width: 15%;
    float: left;
    position: absolute;
    top: 18px;
}
.wom-shr-thanks-dsc{
    margin-top: -45px;
    float: left;
    font-size: 11px;
    max-width:100%;
    padding-left:77px;
    color: #5f7285;
}
.wom-shr-thanks-message {
    width:90%;
    padding-top: 12px;
    font-size: 15px;
    white-space: normal !important;
    line-height: 1.25 !important;
    margin-left:78px !important;
}
#wom-thanks {
    position: relative;
    padding-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
    height: inherit;
    margin: inherit;
    /*   border-top: 1px solid #bdc7c8;*/
    /*   border-bottom: 2px solid #bdc3c7;*/ /*   margin-bottom: 15px;*/
}
