<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
  <title>Wom.by</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/') ?>/css/email/styles.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/') ?>/css/email/responsive.css">
</head>
<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- 100% background wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
  <tr>
    <td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

      <br>

      <!-- 600px container (white background) -->
      <table border="0" width="600" cellpadding="0" cellspacing="0" class="container">
        <tr>
          <td class="container-padding header" align="left">
            Wom.by
          </td>
        </tr>
        <tr>
          <td class="container-padding content" align="left">
            <br>

<div class="title">Welcome to Wom.by</div>
<br>

<div class="body-text">
Welcome to wom.by our dear friend, but first you need to verify your email by simply clicking <a href="<?php echo base_url('user/verify/' . $token) ?>">this link</a>  <br><br>

</div>

<?php $this->load->view('email/footer');?>