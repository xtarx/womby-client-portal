<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=charset=ISO-8859-1">
    <meta charset="charset=ISO-8859-1">
    <title>WOM.by | Client Portal</title>
    <meta name="generator" content="Bootply" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="WOM.by | Client Portal" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>

   <link href="<?php echo base_url('assets/css/') ?>/wom-style.css" rel="stylesheet" >
    <link href="<?php echo base_url('assets/css/') ?>/login.css" rel="stylesheet" >
    <link href="<?php echo base_url('assets/css/') ?>/signup.css" rel="stylesheet" >

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- <link rel="apple-touch-icon" href="/bootstrap/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/bootstrap/img/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/bootstrap/img/apple-touch-icon-114x114.png"> -->
  </head>
  <body>
    <div class="pace-overlay"></div>
    <div class="top-bar swatch-header-white">
      <div class="container">
        <div class="top top-left">
        </div>
        <div class="top top-right">
        </div>
      </div>
    </div>
    <div class="sticky-wrapper" style="height: 91px;"><header class="navbar navbar-static-top navbar-sticky swatch-header-white text-caps navbar-stuck" id="masthead" role="banner">
      <div class="container">
        <div class="navbar-header">
          <button class="navbar-toggle collapsed" data-target=".main-navbar" data-toggle="collapse" data-original-title="" title="">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
          <a href="http://wom.by" class="navbar-brand"><img src="<?php echo base_url('assets/images/') ?>/Logo-Rand-01.png" alt="WOM.by"></a>                </div>


          <nav class="collapse navbar-collapse main-navbar" role="navigation">
            <div class="menu-sidebar pull-right">
            </div>
            <div class="menu-wom-by-one-pager-container">
            <ul id="menu-wom-by-one-pager" class="nav navbar-nav navbar-right">

            <?php if (is_logged()): ?>
            <li id="wom-nav-act">
              <div class="btn-group navbar-btn">
                <button class="btn btn-danger wom-dwn-btn"><?=$this->lang->line("nav_my_account")?></button>
                <button data-toggle="dropdown" class="btn btn-danger wom-dwn-btn dropdown-toggle"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                  
<?php if(has_confirmed()): ?>
                  <li><a href="<?php echo base_url('user/edit') ?>"><?=$this->lang->line("nav_edit")?></a></li>

                  <li><a href="<?php echo base_url('dashboard/choose') ?>"><?=$this->lang->line("nav_platform")?></a></li>
                  <!-- <li><a href="<?php echo base_url('dashboard/charity') ?>">Charity</a></li> -->
<?php endif; ?>

                  <li class="divider"></li>
                  <li><a href="<?php echo base_url('user/logout') ?>"><?=$this->lang->line("nav_logout")?></a></li>
                </ul>
              </div>
            </li>


  <!-- language selector start -->

  <ul  class="nav navbar-nav navbar-right" style="padding-right: 16px">


 <li >
     <!--          <div class=" navbar-btn">

                <button data-toggle="dropdown" class="btn"><?=strtoupper(substr($this->session->userdata('site_lang'), 0, 2));
?><span class="caret"></span></button>
                <ul class="dropdown-menu">
                                  <li><a href="<?php echo base_url('languageSwitcher/switchLang/english') ?>">English</a></li>
                                  <li><a href="<?php echo base_url('languageSwitcher/switchLang/german') ?>">German</a></li>

                </ul>
              </div> -->
            </li>


                      </ul>
  <!-- language selector end -->
            <?php else: ?>

            <li id="menu-item-5592" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5592"><a style="text-transform: uppercase" href="<?php echo base_url('user/login') ?>"><?=$this->lang->line("login_signin")?></a></li>


  <!-- language selector start -->

  <ul  class="nav navbar-nav navbar-right" style="padding-right: 16px">


 <li >
           <!--    <div class=" navbar-btn">

                <button data-toggle="dropdown" class="btn  "><?=strtoupper(substr($this->session->userdata('site_lang'), 0, 2));
?><span class="caret"></span></button>
                <ul class="dropdown-menu">
                                  <li><a href="<?php echo base_url('languageSwitcher/switchLang/english') ?>">English</a></li>
                                  <li><a href="<?php echo base_url('languageSwitcher/switchLang/german') ?>">German</a></li>

                </ul>
              </div> -->
            </li>


                      </ul>
  <!-- language selector end -->
            <?php endif;?>
          </ul>


        </div>
      </nav>
    </div>
  </header>
  </div>

  </br>