<?php

/*
 * English language
 */

// navbar
$lang['nav_my_account'] = 'My Account';
$lang['nav_edit']       = 'Edit & Add billing data';
$lang['nav_platform']   = 'Download & Tutorial';
$lang['nav_clicks']   = 'Clicks';
$lang['nav_charity']    = 'Charity';
$lang['nav_logout']     = 'Logout';

//login
$lang['login_header']          = 'Log In to Your Account';
$lang['login_signin']          = 'Sign in';
$lang['login_create_account']  = 'Create account';
$lang['login_forgot_password'] = 'Forgot password';

//signup page

$lang['signup_intro']              = 'Please enter your contact data to create your profile and download the WOM.by Tool';
$lang['signup_charity_intro']      = 'Please enter your contact data to create your profile and download the WOM.by Tool';
$lang['signup_domain_name']        = 'Domain Name';
$lang['signup_store_url']          = 'Store Website URL';
$lang['signup_personal_data']      = 'Personal Data';
$lang['signup_password']           = 'Password';
$lang['signup_confirm_password']   = 'Confirm Password';
$lang['signup_confirm_email']      = 'Comfirm Email';
$lang['signup_billing_data']       = 'Company Data';
$lang['signup_company_name']       = 'Company Name';
$lang['signup_street_name']        = 'Street';
$lang['signup_postal_code']        = 'Postal Code';
$lang['signup_city']               = 'City';
$lang['signup_country']            = 'Country';
$lang['signup_vat']                = 'VAT ID';
$lang['signup_account_name']       = 'Account Holder Name';
$lang['signup_bank_name']          = 'Bank Name';
$lang['signup_phone_number']       = 'Phone Number';
$lang['signup_accept_1']           = 'I accept';
$lang['signup_accept_terms']       = 'Terms and Conditions';
$lang['signup_accept_price']       = 'Pricing Terms';
$lang['signup_accept_2']           = '';
$lang['signup_continue']           = 'Download WOM.by';
$lang['signup_sepa_title']         = 'SEPA mandate';
$lang['signup_sepa_title_charity'] = 'SEPA-Lastschriftmandat für Spenden';

$lang['signup_pick_charity'] = 'Wähle eine unserer Partnerorganisationen';
$lang['signup_sepa_agree']   = 'I agree to electronically transmit SEPA-mandate PDF';

// dashboard choose
$lang['dashboard_available']                     = 'AVAILABLE';
$lang['dashboard_woo_install']                   = 'Discount Tool';
$lang['dashboard_woo_install_charity']      = 'Charity Tool';
$lang['dashboard_magento_install']               = 'Word of Mouth for Magento';
$lang['dashboard_shopware_install']              = 'Word of Mouth for Shopware5';
$lang['dashboard_shopware_charity']              = 'Charity Tool for Shopware5';
$lang['dashboard_install_guide']                 = 'Installation Guide';
$lang['dashboard_facebook']                      = 'How to Create a Facebook ID';
$lang['dashboard_sepa']                          = 'SEPA Mandate';
$lang['dashboard_filled']                        = 'Thank you, you can now use womby';
$lang['dashboard_shopware_charity_fill_charity'] = 'Charity Registrierung';
//edit
$lang['edit_payment_warning'] = 'SEPA Mandate';

//charity
$lang['charity_terms']       = 'ich stimme den Ausschlusskriterien zur Nutzung zu';
$lang['charity_terms_label'] = 'Ausschlusskriterien zur Nutzun';
$lang['charity_terms_title'] = 'Damit	Sie	mit	den	Organisationen	zusammenarbeiten	können,	müssen	Sie	bestätigen,	dass	sie	keine	der	folgenden	Produktgruppen	verkaufen';
