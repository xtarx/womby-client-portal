<?php

/*
 * German language
 */

// navbar
$lang['nav_my_account'] = 'Mein Konto';
$lang['nav_edit']       = 'Konto bearbeiten & Rechnungsdaten';
$lang['nav_platform']   = 'Download & Tutorial';
$lang['nav_clicks']   = 'Klicks';
$lang['nav_charity']    = 'Charity';
$lang['nav_logout']     = 'Abmelden';

$lang['login_header']          = 'In Ihr Konto einloggen';
$lang['login_signin']          = 'Einloggen';
$lang['login_create_account']  = 'Konto erstellen';
$lang['login_forgot_password'] = 'Passwort vergessen';

//signup page
$lang['signup_intro']         = 'Herzlich Willkommen bei der Anmeldung & Installation von WOM.by. Bitte geben Sie ihre Geschäfts- & Bankdaten ein, um WOM.by zu installieren.';
$lang['signup_charity_intro'] = 'Herzlich Willkommen bei der Anmeldung & Installation von Charity-WOM.by. Bitte geben Sie
ihre Geschäfts- & Bankdaten ein, um Charity-WOM.by zu installieren.';
$lang['signup_domain_name']      = 'Domain Name';
$lang['signup_store_url']        = 'URL Ihres Shops';
$lang['signup_personal_data']    = 'Anmeldedaten';
$lang['signup_password']         = 'Passwort';
$lang['signup_confirm_password'] = 'Bestätige das Passwort';
$lang['signup_confirm_email']    = 'Bestätigungs-E-Mail';
$lang['signup_billing_data']     = 'Rechnungsdaten';
$lang['signup_company_name']     = 'Firmen Name';
$lang['signup_street_name']      = 'Straße';
$lang['signup_postal_code']      = 'PLZ';
$lang['signup_city']             = 'Stadt';
$lang['signup_country']          = 'Land';
$lan['signup_vat']               = 'VAT ID';

$lang['signup_account_name']       = 'Kontoinhaber';
$lang['signup_bank_name']          = 'Bank';
$lang['signup_phone_number']       = 'Telefon';
$lang['signup_accept_1']           = 'Ich stimme';
$lang['signup_accept_terms']       = 'den allgemeine	Nutzungsbedingungen';
$lang['signup_accept_price']       = 'Preiskonditionen';
$lang['signup_accept_2']           = 'zu';
$lang['signup_continue']           = 'weiter zum nächsten Schritt...';
$lang['signup_sepa_title']         = 'SEPA-Lastschriftmandat';
$lang['signup_sepa_title_charity'] = 'SEPA-Lastschriftmandat für Spenden';
$lang['signup_pick_charity']       = 'Wähle eine unserer Partnerorganisationen';
$lang['signup_sepa_agree']         = 'Ich stimme der elektronischen Übermittlung des SEPA-Lastschriftmandats zu.';
// dashboard choose
$lang['dashboard_available']        = 'VERFÜGBAR';
$lang['dashboard_woo_install']      = 'Discount Tool';
$lang['dashboard_woo_install_charity']      = 'Charity Tool';

$lang['dashboard_magento_install']  = 'WOM.by Magento Plugin';
$lang['dashboard_shopware_install'] = 'WOM.by Shopware5 Plugin';
$lang['dashboard_shopware_charity'] = 'Charity Tool for Shopware5';

$lang['dashboard_shopware_charity_fill_charity'] = 'Charity Registrierung';

$lang['dashboard_install_guide'] = 'Tutorial – WOM.by installieren';
$lang['dashboard_facebook']      = 'Tutorial - Facebook ID erstellen';
$lang['dashboard_sepa']          = 'Sepa Mandat';
$lang['dashboard_filled']        = 'Vielen Dank! Nun können Sie passend für Ihre System Charity-WOM.by herunterladen und
installieren.';

//edit
$lang['edit_payment_warning'] = 'SEPA Mandate';

//charity
$lang['charity_terms']       = 'ich stimme den Ausschlusskriterien zur Nutzung zu';
$lang['charity_terms_label'] = 'Ausschlusskriterien zur Nutzun';
$lang['charity_terms_title'] = 'Damit	Sie	mit	den	Organisationen	zusammenarbeiten	können,	müssen	Sie	bestätigen,	dass	sie	keine	der	folgenden	Produktgruppen	verkaufen';
