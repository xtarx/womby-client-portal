<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Activator extends REST_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // $this->methods['user_get']['limit']    = 500; // 500 requests per hour per user/key
        // $this->methods['user_post']['limit']   = 100; // 100 requests per hour per user/key
        // $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->model('CompanyWebsites_model');
        $this->load->model('Installs_info_model');
    }

    public function index_get()
    {

        // $slack_msg = '"' . "neasdadw shareonproduct#asda" . '"';

        // echo "text is " . '{"channel": "#lovelyshares", "username": "womby-tracker",' . '"text":' . $slack_msg . ', "icon_emoji": ":metal:"}';
        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        //     CURLOPT_RETURNTRANSFER => 1,
        //     CURLOPT_URL            => 'https://hooks.slack.com/services/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',
        //     CURLOPT_USERAGENT      => 'Codular Sample cURL Request',
        //     CURLOPT_POST           => 1,
        //     CURLOPT_POSTFIELDS     => array(
        //         'payload' => '{"channel": "#lovelyshares", "username": "womby-tracker",' . '"text":' . $slack_msg . ', "icon_emoji": ":metal:"}',
        //     ),
        // ));
        // $resp = curl_exec($curl);
        // curl_close($curl);
        $this->db->cache_delete_all();

        return;

    }
    public function check_post()
    {
        // Users from a data store e.g. database

        //COMMENT ME IF U WANT TO DISABLE "ALL ACTIVE" BY DEFAULT

        $this->set_response('active', REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        return;

<<<<<<< HEAD
        //if ($this->CompanyWebsites_model->is_subdomain($this->post('url'))) {
        //    // echo "is sub-domain";
        //    $hashed = $this->CompanyWebsites_model->hash_subdomain($this->post('url'));
        //} else {
        //    $hashed = $this->CompanyWebsites_model->hash($this->post('url'));
        //}
        // echo "url is " . $this->post('url');
        // echo "hashed " . $hashed;
        //$this->db->cache_on();
        //$all_active_hashes = $this->CompanyWebsites_model->get_many_by(array('enabled' => 1));

        //$key = whatever($all_active_hashes, "hash", $hashed);

        //if ($key) {
        //    $this->set_response('active', REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        //} else {
        //    $this->set_response([
        //        'status'  => false,
        //        'message' => 'URL could not be found',
        //    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        //}
=======
        //COMMENT END HERE


        // if ($this->CompanyWebsites_model->is_subdomain($this->post('url'))) {
        //     // echo "is sub-domain";
        //     $hashed = $this->CompanyWebsites_model->hash_subdomain($this->post('url'));
        // } else {
        //     $hashed = $this->CompanyWebsites_model->hash($this->post('url'));
        // }
        // // echo "url is " . $this->post('url');
        // // echo "hashed " . $hashed;
        
        // $all_active_hashes = $this->CompanyWebsites_model->get_many_by(array('enabled' => 1));

        // $key = whatever($all_active_hashes, "hash", $hashed);

        // if ($key) {
        //     $this->set_response('active', REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        // } else {
        //     $this->set_response([
        //         'status'  => false,
        //         'message' => 'URL could not be found',
        //     ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        // }


        
>>>>>>> 11f30bacb898b7395149bde9ac4002b93e550825
    }

    public function installed_post()
    {

        $this->Installs_info_model->insert(array(
            'url'            => $this->post('url'),
            'version_plugin' => $this->post('version_plugin'),
            'platform'       => $this->post('platform'),

        ));

        $this->set_response('active', REST_Controller::HTTP_OK);

    }
}
