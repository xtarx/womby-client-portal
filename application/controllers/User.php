<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('countries_model');
        $this->load->model('companies_model');
        $this->load->model('payments_model');
        $this->load->model('UserEdits_model');
        $this->load->model('companyWebsites_model');
    }

    public function migrate()
    {

        $urls = $this->companyWebsites_model->get_all();

        foreach ($urls as $web) {

            // print_r($web);
            $hash = $this->companyWebsites_model->hash($web['url']);
            $this->companyWebsites_model->update($web['id'], array(
                'hash' => $hash,
            ));

        }

    }

    public function index()
    {
        if (is_logged()) {
            redirect_home();
        }
        redirect_login();

    }

    public function edit()
    {

        if (!is_logged()) {
            $this->session->set_userdata('last_page', current_url());

            redirect_login();
        }

        // echo "a7a";
        $user_id         = $this->session->userdata('user_id');
        $user_details    = $this->users_model->get($user_id);
        $company_details = $this->companies_model->get_by(array('user_id' => $user_id));
        // print_r($user_details);

        $payment_details = $this->payments_model->get_by(array('user_id' => $user_id));

        $show_warning = false;

        if (!$payment_details) {
            $show_warning    = true;
            $payment_details = array(
                'b_account_holder_name' => "",
                'b_name'                => "",
                'b_IBAN'                => "",
                'b_BIC'                 => "",
            );
        }
        $data = array_merge($user_details, $company_details, $payment_details);

        $data['show_warning'] = false;

        if ($show_warning) {

            $data['show_warning'] = true;
        }

        // $data     = array_merge($user_details, $company_details);
        $websites = $this->companyWebsites_model->get_many_by(array('user_id' => $user_id));

        // print_r($data);

        $data['title']     = 'title';
        $data['websites']  = $websites;
        $data['countries'] = $this->countries_model->order_by('priority', 'DESC')->order_by('id', 'ASC')->get_all();

        //if has charity
        if ($user_details['charity_token']) {
            $this->load->model('charities_model');
            $data['charities']        = $this->charities_model->get_all();
            $data['selected_charity'] = $user_details['selected_charity'];
        }
        // print_r($data);
        $this->load->view('user/edit', $data);
    }

    public function signup($charity = false)
    {

        $data['title']     = 'title';
        $data['countries'] = $this->countries_model->order_by('priority', 'DESC')->order_by('id', 'ASC')->get_all();

        if ($charity) {
            $this->session->set_userdata('charity_signup', 1);
        } else {
            $this->session->unset_userdata('charity_signup');

        }

        $this->load->view('user/signup', $data);
    }

    public function confirm($edit = false)
    {
        if (!is_logged()) {
            redirect_login();
        }

        if ($edit) {
            $data = $this->session->flashdata('signup_data');

            $data['edit'] = "edit";
        } else {
            $data         = $this->session->userdata('signup_data_2');
            $data['edit'] = false;

        }
        $this->load->view('user/signup_2', $data);
    }

    public function forgot_password()
    {

        if (is_logged()) {
            redirect_home();
        }

        $data['page_title'] = "";

        $this->load->view("user/forgot", $data);
    }

    public function do_forgot()
    {

        // $response = $this->users_model->get_by(array('email' => $this->input->post('email'), 'enabled' => 1));
        $response = $this->users_model->get_by(array('email' => $this->input->post('email')));
        if ($response) {

            $user_id = $response['id'];
            // Set reset datetime
            $time = $this->users_model->update_reset_sent_datetime($user_id);

            // Generate reset password url
            $password_reset_url = site_url('user/reset_password?id=' . $user_id . '&token=' . sha1($user_id . $time . $this->config->item('password_reset_secret')));

            $html = "<a href='" . $password_reset_url . "'>" . $password_reset_url . "<a>";

            sendmail($response['email'], "Reset Password", $html);
            $this->session->set_flashdata('success_msg', "Password reset link has been sent, check your mail");

            $this->load->view("user/forgot");

            // echo $password_reset_url;

        } else {

            $data['error'] = "Make sure the email is correct and verified";
            $this->load->view("user/forgot", $data);

        }

    }

    public function reset_password()
    {
        // Get account by email
        $account = $this->users_model->get($this->input->get('id'));
        if ($account) {

            // Check if reset password has expired
            if (now() < (strtotime($account['resetsenton']) + $this->config->item("password_reset_expiration"))) {

                // echo "a7a";
                // Check if token is valid
                if ($this->input->get('token') == sha1($account['id'] . strtotime($account['resetsenton']) . $this->config->item('password_reset_secret'))) {
                    // Remove reset sent on datetime
                    $this->users_model->remove_reset_sent_datetime($account['id']);
                    $this->sign_user_in($account);

                }
            }

            // redirect_home();

        }
        redirect_login();

    }
    public function verify($token)
    {

        $user = $this->users_model->get_by(array('token' => $token, 'enabled' => 0));

        if ($user) {

            $this->users_model->update($user['id'], array('enabled' => 1));
            $this->session->set_flashdata('success_msg', "Successfully verified please login");

            redirect_login();
        } else {

            echo "Wrong Token or Account Has been verified before";
        }
    }

    public function sign_user_in($response, $no_redirect = false)
    {

        $session_data = array(
            'user_id' => $response['id'],
            'email'   => $response['email'],
            'token'   => $response['token'],

        );
        // $this->users_model->update_last_signed_in_datetime($response['id']);

        $this->session->set_userdata($session_data);
        if (!$no_redirect) {
            redirect_edit();
        }

    }
    public function login()
    {

        if (is_logged()) {
            redirect_home();
        }

        $data['title'] = 'title';
        $this->load->view('user/login', $data);

    }

    public function logout()
    {

        $this->session->sess_destroy();

        redirect_login();
    }

    public function do_login()
    {
        if (is_logged()) {
            redirect_home();
        }

        // Get parameters
        $email    = $this->input->post('email', true);
        $password = $this->input->post('password', true);

        // print_r($_POST);
        // PHP validation check
        $this->form_validation->set_rules('email', null, 'required|valid_email');
        $this->form_validation->set_rules('password', null, 'required');
        if ($this->form_validation->run() == false) {

            $this->login();
            return;
        }
        $password = sha1($password);

        // $response = $this->users_model->get_by(array('email' => $email, 'password' => $password, 'enabled' => 1));
        $response = $this->users_model->get_by(array('email' => $email, 'password' => $password));
        if ($response) {

            $this->sign_user_in($response, true);

            $last_page = $this->session->userdata('last_page');
            if ($last_page) {
                redirect($last_page);
            } else {
                redirect_home();
            }
        } else {
            $data['redirection'] = $this->input->post('redirection', true);
            $data['error']       = "Make sure email and password are correct";

            $this->load->view("user/login", $data);

        }

    }

    public function generate_pdf_manually($user_id)
    {

        $user_details = $this->users_model->get($user_id);

        $company_details = $this->companies_model->get_by(array('user_id' => $user_id));

        $payment_details = $this->payments_model->get_by(array('user_id' => $user_id));

        //append token & customer ID
        $payment_details['token']   = $user_details['token'];
        $payment_details['user_id'] = $user_id;

        $details = array_merge($company_details, $payment_details);
        $this->generate_pdf($details);

    }
    public function generate_pdf($data)
    {

        $data['date'] = date('d-m-Y');

        // print_r($data);
        $this->pdf->load_view('templates/sepa', $data, true);
        $this->pdf->render();
        $output = $this->pdf->output();
        file_put_contents('generated_pdfs/' . $data['token'] . '.pdf', $output);

    }

    public function do_edit()
    {

        $user_id = $this->session->userdata('user_id');
        if ($this->session->userdata('email') != $this->input->post('email')) {

            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users . email]|matches[confirm_email]');
        }

        $this->form_validation->set_rules('confirm_email', 'Email Confirmation', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', 'PasswordConfirmation', 'required');

        $this->form_validation->set_rules('company_name', 'Company name', 'required');
        $this->form_validation->set_rules('company_url[]', 'Store URL', 'required|valid_url');
        $this->form_validation->set_rules('street', 'streetname', 'required');
        $this->form_validation->set_rules('postal_code', 'postal_code', 'required');
        $this->form_validation->set_rules('city', 'city', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('country', 'country', 'required');

        $this->form_validation->set_rules('account_holder_name', 'account_holder_name', 'required');
        $this->form_validation->set_rules('bank_name', 'b_name', 'required');
        $this->form_validation->set_rules('bank_iban', 'b_IBAN', 'required');
        $this->form_validation->set_rules('bank_bic', 'b_BIC', 'required');

        $this->form_validation->set_rules('confirmation_1', 'confirmation_1', 'required');
        if ($this->form_validation->run() == false) {

            redirect('user/edit');
        }

        //send verification
        // $this->users_model->send_verification($user_info);

        $details = array(
            'user_id'               => $user_id,
            'email'                 => $this->input->post('email'),
            'password'              => sha1($this->input->post('password')),
            'company_name'          => $this->input->post('company_name'),
            'company_url'           => implode(", ", $this->input->post('company_url[]')),
            'street'                => $this->input->post('street'),
            'postal_code'           => $this->input->post('postal_code'),
            'city'                  => $this->input->post('city'),
            'country'               => $this->input->post('country'),
            'phone'                 => $this->input->post('phone'),
            'b_account_holder_name' => $this->input->post('account_holder_name'),
            'b_name'                => $this->input->post('bank_name'),
            'b_IBAN'                => $this->input->post('bank_iban'),
            'b_BIC'                 => $this->input->post('bank_bic'),

        );

        $response     = $this->UserEdits_model->insert($details);
        $user_details = $this->users_model->get($user_id);

        //if charity
        if ($this->input->post('charity')) {

            $this->users_model->update($user_id, array(
                'selected_charity' => $this->input->post('charity'),
            ));

            // $webs = $this->companyWebsites_model->get_many_by(array('user_id' => $user_id));

            $this->companyWebsites_model->update_by(array('user_id' => $user_id), array(
                'selected_charity' => $this->input->post('charity'),
            ));

        }
        //if charity

        $details['token'] = $user_details['token'];
        if ($response) {

            $this->session->set_userdata("name", $this->input->post('account_holder_name'));
            // $this->generate_pdf($details);

            $this->session->set_flashdata('signup_data', $details);

            $data['title'] = "Changing info request Wom.by";
            $data['body']  = "Your request will be applied once approved by our side, have a nice day!";
            $data['email'] = $details['email'];
            // $this->users_model->send_text($data);
            $this->session->set_userdata("reg_user_id", $user_id);

            $this->do_approve_edit($user_id);
            redirect('user/confirm/edit');

        } else {
            echo validation_errors();
        }

    }
    public function do_approve_edit($user_id)
    {

        $details = $this->UserEdits_model->order_by('created_at', 'desc')->get_by(array('user_id' => $user_id));

        // print_r($details);
        // exit();
        $user_info = $this->users_model->update($user_id, array(
            'email'    => $details['email'],
            'password' => $details['password'],
        ));

        //send verification
        // $this->users_model->send_verification($user_info);

        $company_details = array(
            'user_id'      => $user_id,
            'company_name' => $details['company_name'],
            // 'company_url'  => $details['company_url'],
            'street'       => $details['street'],
            'postal_code'  => $details['postal_code'],
            'city'         => $details['city'],
            'country'      => $details['country'],
            'phone'        => $details['phone'],

        );

        //insert url(s)

        $this->companyWebsites_model->delete_by(array('user_id' => $user_id));

        $webs = explode(", ", $details['company_url']);
        // print_r($webs);
        foreach ($webs as $web) {

            $hash = $this->companyWebsites_model->hash($web);
            $this->companyWebsites_model->insert(array(
                'user_id' => $user_id,
                'url'     => $web,
                'hash'    => $hash,
            ));

        }

        $response = $this->companies_model->update_by(array('user_id' => $user_id), $company_details);

        $payment_details = array(
            'user_id'               => $user_id,
            'b_account_holder_name' => $details['b_account_holder_name'],
            'b_name'                => $details['b_name'],
            'b_IBAN'                => $details['b_IBAN'],
            'b_BIC'                 => $details['b_BIC'],

        );

        $response = $this->payments_model->update_by(array('user_id' => $user_id), $payment_details);

        // echo "Confirmed & send mail to client";

        // $data['title'] = "Change request approved Wom.by";
        // $data['body']  = "Your request was approved by our side, have a nice day!";
        // $data['email'] = $details['email'];
        // $this->users_model->send_text($data);
    }

    public function do_signup()
    {

        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users . email]|matches[confirm_email]');
        $this->form_validation->set_rules('confirm_email', 'Email Confirmation', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');
        $this->form_validation->set_rules('confirm_password', 'PasswordConfirmation', 'required');
        $this->form_validation->set_rules('company_url[]', 'Store URL', 'required|valid_url');
        $this->form_validation->set_rules('country', 'country', 'required');

        if ($this->form_validation->run() == false) {

            echo "form validation failed";
            $data['title'] = 'title';
            $this->load->view('user/signup', $data);
            return;
        }

        $user_info = array(
            'email'    => $this->input->post('email'),
            'password' => sha1($this->input->post('password')),
            'token'    => generate_token(),

        );
        $user_id         = $this->users_model->insert($user_info);
        $user_info['id'] = $user_id;

        //send verification

        // $this->users_model->send_verification($user_info);

        //insert url(s)
        $webs = $this->input->post('company_url[]');
        foreach ($webs as $web) {
            $hash = $this->companyWebsites_model->hash($web);
            $this->companyWebsites_model->insert(array(
                'user_id' => $user_id,
                'url'     => $web,
                'hash'    => $hash,
            ));

        }

        if ($user_id) {

            $this->session->set_userdata("name", $this->input->post('account_holder_name'));

            $this->session->set_userdata('signup_data_1', $user_info);
            // print_r($user_info);
            $this->session->set_userdata("reg_user_id", $user_id);

            //sign user in

            $user_info['id'] = $user_id;
            $this->sign_user_in($user_info, "no-redirect");

            //if  SEPA
            $country_selected = $this->input->post('country');
            $sepa_countries   = ["Austria", "Belgium", "Bulgaria", "Croatia", "Czech Republic", "Cyprus", "Denmark", "Estonia", "Finland", "France", "Germany", "Greece", "Hungary", "Ireland", "Italy", "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Slovakia", "Slovenia", "Spain", "Sweden", "United Kingdom"];
            // echo "country selected  ".$country_selected;

            if (in_array($country_selected, $sepa_countries)) {

                redirect('user/singup_billing');
            } else {
                //non sepa
                redirect('user/singup_billing_non_sepa');

            }

        }

    }

    public function singup_billing()
    {
        if (!is_logged()) {
            redirect_login();
        }

        $data['title']     = 'title';
        $data['countries'] = $this->countries_model->order_by('priority', 'DESC')->order_by('id', 'ASC')->get_all();

        $this->load->view('user/signup_billing', $data);
    }
 public function singup_billing_non_sepa()
    {
        if (!is_logged()) {
            redirect_login();
        }

        $data['title']     = 'title';
        $data['countries'] = $this->countries_model->order_by('priority', 'DESC')->order_by('id', 'ASC')->get_all();

        $this->load->view('user/singup_billing_non_sepa', $data);
    }

    public function do_signup_billing()
    {

        $this->form_validation->set_rules('company_name', 'Company name', 'required');
        $this->form_validation->set_rules('street', 'streetname', 'required');
        $this->form_validation->set_rules('postal_code', 'postal_code', 'required');
        $this->form_validation->set_rules('city', 'city', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('country', 'country', 'required');

        //remove payment to another page
        $this->form_validation->set_rules('account_holder_name', 'account_holder_name', 'required');
        $this->form_validation->set_rules('bank_name', 'b_name', 'required');
        $this->form_validation->set_rules('bank_iban', 'b_IBAN', 'required');
        $this->form_validation->set_rules('bank_bic', 'b_BIC', 'required');
        if ($this->form_validation->run() == false) {

            echo "form validation failed";
            echo validation_errors();
            redirect('user/singup_billing');
            return;
        }

        $user_info = $this->session->userdata('signup_data_1');

        // print_r($user_info);
        $company_details = array(
            'user_id'      => $user_info['id'],
            'company_name' => $this->input->post('company_name'),
            'street'       => $this->input->post('street'),
            'postal_code'  => $this->input->post('postal_code'),
            'city'         => $this->input->post('city'),
            'country'      => $this->input->post('country'),
            'phone'        => $this->input->post('phone'),
        );

        $response = $this->companies_model->insert($company_details);
        //remove payment to another page
        $payment_details = array(
            'user_id'               => $user_info['id'],
            'b_account_holder_name' => $this->input->post('account_holder_name'),
            'b_name'                => $this->input->post('bank_name'),
            'b_IBAN'                => $this->input->post('bank_iban'),
            'b_BIC'                 => $this->input->post('bank_bic'),
            'vat'                   => $this->input->post('vat'),
        );
        $response = $this->payments_model->insert($payment_details);

        //append token & customer ID
        $payment_details['token']   = $user_info['token'];
        $payment_details['user_id'] = $user_info['id'];
        if ($response) {

            $this->session->set_userdata("name", $this->input->post('account_holder_name'));

            $details = array_merge($company_details, $payment_details);
            $this->generate_pdf($details);

            $this->session->set_userdata('signup_data_2', $details);

            //remove payment to another page
            // redirect('user/confirm');
            $charity_signup = $this->session->userdata('charity_signup');

            redirect('user/confirm/');

        } else {
            echo validation_errors();
        }

    }

    public function do_confirm($edit = false)
    {

        $this->form_validation->set_rules('confirmation_2', 'confirmation_2', 'required');
        if ($this->form_validation->run() == false) {

            $this->load->view('user/signup_2');
            return;
        }

        if ($edit) {
            // echo "a8a";
            $this->session->set_flashdata('warnings', "Your changes will be applied once confirmed by our side, thank you!");
        }
        //
        $user_id = $this->session->userdata('reg_user_id');
        $this->users_model->update($user_id, array('filled' => 1));
        $this->session->unset_userdata('reg_user_id');
        //delete cache
        $this->db->cache_delete_all();

        if ($this->session->userdata('charity_signup')) {
            redirect('dashboard/charity');
            $this->session->unset_userdata('charity_signup');

        } else {
            redirect('dashboard/choose');
        }

    }

}
