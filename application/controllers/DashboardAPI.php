<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class DashboardAPI extends REST_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit']    = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit']   = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->model('users_model');
        $this->load->model('countries_model');
        $this->load->model('CompanyWebsites_model');
        $this->load->model('CharityWebsiteCategories_model');
        $this->load->model('charityShopCategories_model');
        $this->load->model('companies_model');
        $this->load->model('payments_model');
        $this->load->model('UserEdits_model');
        $this->load->model('CompanyWebsites_model');
    }

    public function list_get()
    {

        $charities = [
            ['id' => 1, 'name' => 'Welthungerhilfe', 'url' => 'http://en.unesco.org/', 'logo' => base_url('assets/images/Welthungerhilfe.png')],
        ];

        if ($charities) {
            $this->set_response($charities, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $this->set_response([
                'status'  => false,
                'message' => 'URL could not be found',
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

}
