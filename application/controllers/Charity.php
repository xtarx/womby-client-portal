<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Charity extends REST_Controller
{

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit']    = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit']   = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        // $this->load->model('CompanyWebsites_model');
    }

    public function list_get()
    {


        $this->load->model('charities_model');
        $this->load->model('CompanyWebsites_model');
        $this->load->model('users_model');


        if (!array_key_exists("HTTP_REFERER", $_SERVER)) {
            // echo "a7a";
            $dum[] = array(
                'name' => "Aktion Deutschland Hilft e.V.",
                'url'  => "https://charity.wom.by/aktiondeutschlandhilft/",
                'logo' => 'https://portal-dot-even-electron-151909.appspot.com/assets/images/ADH72dpi.png',
            );
            $this->set_response($dum, REST_Controller::HTTP_OK);
            return;
        }

        $url = $_SERVER['HTTP_REFERER'];

        if ($this->CompanyWebsites_model->is_subdomain($url)) {
            $hashed = $this->CompanyWebsites_model->hash_subdomain($url);
        } else {
            $hashed = $this->CompanyWebsites_model->hash($url);
        }

        $website      = $this->CompanyWebsites_model->get_by(array('hash' => $hashed));
        $user_details = $this->users_model->get($website['user_id']);

        $selected_charity = $user_details['selected_charity'];
        $charity          = $this->charities_model->get($website['selected_charity']);

        // exit();
        if ($charity) {
            $charity['logo']  = 'https://portal-dot-even-electron-151909.appspot.com/assets/images/' . $charity['logo'];
            $charities[]      = $charity;

            $this->set_response($charities, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        } else {
            $dum[] = array(
                'name' => "Aktion Deutschland Hilft e.V.",
                'url'  => "http://charity.wom.by/aktiondeutschlandhilft/",
                'logo' => 'https://portal-dot-even-electron-151909.appspot.com/assets/images/ADH72dpi.png',
            );
            $this->set_response($dum, REST_Controller::HTTP_OK);
        }
    }
    public function list_magento_post()
    {

        $this->load->model('charities_model');
        $this->load->model('CompanyWebsites_model');
        $this->load->model('users_model');

        $url = $this->input->post('url');

        if ($this->CompanyWebsites_model->is_subdomain($url)) {
            $hashed = $this->CompanyWebsites_model->hash_subdomain($url);
        } else {
            $hashed = $this->CompanyWebsites_model->hash($url);
        }

        $website      = $this->CompanyWebsites_model->get_by(array('hash' => $hashed));
        $user_details = $this->users_model->get($website['user_id']);

        $selected_charity = $user_details['selected_charity'];
        $charity          = $this->charities_model->get($website['selected_charity']);
        $charities        = false;
        if ($charity) {
            $charity['logo'] = 'https://portal-dot-even-electron-151909.appspot.com/assets/images/' . $charity['logo'];
            $charities[]     = $charity;
        }
        if ($charities) {
            $this->set_response($charities, REST_Controller::HTTP_OK);
        } else {
            $dum[] = array(
                'name' => "Aktion Deutschland Hilft e.V.",
                'url'  => "http://charity.wom.by/aktiondeutschlandhilft/",
                'logo' => 'https://portal-dot-even-electron-151909.appspot.com/assets/images/ADH72dpi.png',
            );
            $this->set_response($dum, REST_Controller::HTTP_OK);
        }
    }

}
