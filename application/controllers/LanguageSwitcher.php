<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class LanguageSwitcher extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function switchLang($language = "")
    {

        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);

        if (isset($_SERVER['HTTP_REFERER'])) {
            redirect($_SERVER['HTTP_REFERER']);

        } else {
            //it was not sent, perform your default actions here
            redirect_login();
        }

    }
}
