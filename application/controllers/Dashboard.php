<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('countries_model');
        $this->load->model('CompanyWebsites_model');
        $this->load->model('CharityWebsiteCategories_model');
        $this->load->model('charityShopCategories_model');
        $this->load->model('companies_model');
        $this->load->model('payments_model');
        $this->load->model('UserEdits_model');
        $this->load->model('charities_model');
        
    }

    public function css_magento()
    {

        if (array_key_exists("HTTP_REFERER", $_SERVER)) {

            if (false !== stripos($_SERVER['HTTP_REFERER'], "magento107.wom.by")) {
                $this->load->view('css_fly/magento/magento_custom_wom');
                return;
            }

            if (false !== stripos($_SERVER['HTTP_REFERER'], "stilbag.com")) {
                $this->load->view('css_fly/magento/magento_stillbag_wom');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "ferengo.99hero.de")) {
                $this->load->view('css_fly/magento/magento_ferengo99hero_wom');
                return;
            }if (false !== stripos($_SERVER['HTTP_REFERER'], "starterstore.de")) {
                $this->load->view('css_fly/magento/magento_starterstore_wom');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "badartikel.de")) {
                $this->load->view('css_fly/magento/magento_badartikel_wom');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "knocknok-fashion.com")) {
                $this->load->view('css_fly/magento/magento_knocknok_wom');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "karry24.de")) {
                $this->load->view('css_fly/magento/magento_karry24_de_wom');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "trachtenheimat.de")) {
                $this->load->view('css_fly/magento/magento_trachtenheimat_de_wom');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "scheurichweine.de")) {
                $this->load->view('css_fly/magento/magento_scheurichweine_de_wom');
                return;
            }
        
        }
        $this->load->view('css_fly/magento/magento_default_wom');

    }

    public function css_shopware4()
    {

        if (array_key_exists("HTTP_REFERER", $_SERVER)) {

            if (false !== stripos($_SERVER['HTTP_REFERER'], "oberland.la")) {
                $this->load->view('css_fly/shopware4/sw_oberland');
                return;
            }

        }

        $this->load->view('css_fly/shopware4/sw_4_default');

    }

    public function css_shopware5()
    {

        if (array_key_exists("HTTP_REFERER", $_SERVER)) {

            if (false !== stripos($_SERVER['HTTP_REFERER'], "shopware102.wom.by")) {
                $this->load->view('css_fly/shopware5/sw_custom_wom');
                return;
            }

            if (false !== stripos($_SERVER['HTTP_REFERER'], "dufner-shop.de")) {
                $this->load->view('css_fly/shopware5/sw_5_dufner-shopde');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "dufner-schuhe.de")) {
                $this->load->view('css_fly/shopware5/sw_5_dufner-shopde');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "waldlaeufershop.de")) {
                $this->load->view('css_fly/shopware5/sw_5_dufner-shopde');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "laclock.de")) {
                $this->load->view('css_fly/shopware5/sw_5_laclock');
                return;
            }

        }
        $this->load->view('css_fly/shopware5/sw_5_default');

    }
    public function css_charity_shopware4()
    {

        if (array_key_exists("HTTP_REFERER", $_SERVER)) {

            if (false !== stripos($_SERVER['HTTP_REFERER'], "laclock.de")) {
                // $this->load->view('css_fly/shopware5/sw_5_laclock');
                return;
            }

        }
        $this->load->view('css_fly/shopware4/charity/sw_4_charity_default');

    }

    public function css_charity_shopware5()
    {

        if (array_key_exists("HTTP_REFERER", $_SERVER)) {

            if (false !== stripos($_SERVER['HTTP_REFERER'], "dufner-shop.de")) {
                $this->load->view('css_fly/shopware5/charity/dufner-shop');
                return;
            }
        if (false !== stripos($_SERVER['HTTP_REFERER'], "dufner-schuhe.de")) {
                $this->load->view('css_fly/shopware5/charity/dufner-shop');
                return;
            }
            
            if (false !== stripos($_SERVER['HTTP_REFERER'], "waldlaeufershop.de")) {
                $this->load->view('css_fly/shopware5/charity/dufner-shop');
                return;
            }
        }
        $this->load->view('css_fly/shopware5/charity/sw_5_charity_default');

    }

    public function css_charity_magento()
    {

        if (array_key_exists("HTTP_REFERER", $_SERVER)) {

                      
                  
            if (false !== stripos($_SERVER['HTTP_REFERER'], "magento107.wom.by")) {
                $this->load->view('css_fly/magento/charity/magento101');
                return;
            }
             
             if (false !== stripos($_SERVER['HTTP_REFERER'], "ferengo.99hero.de")) {
                $this->load->view('css_fly/magento/charity/ferengode');
                return;
            }
            
            if (false !== stripos($_SERVER['HTTP_REFERER'], "ferengo.de")) {
                $this->load->view('css_fly/magento/charity/ferengode');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "stilbag.com")) {
                $this->load->view('css_fly/magento/charity/stilbag_com');
                return;
            }
            if (false !== stripos($_SERVER['HTTP_REFERER'], "frisoerschaefer.de")) {
                $this->load->view('css_fly/magento/charity/frisoerschaeferde');
                return;
            }
        }
        $this->load->view('css_fly/magento/charity/magento_charity_default');

    }


    public function css_charity_woo()
    {

        if (array_key_exists("HTTP_REFERER", $_SERVER)) {

            if (false !== stripos($_SERVER['HTTP_REFERER'], "dufner-shop.de")) {
                $this->load->view('css_fly/woo/charity/dufner-shop');
                return;
            }
    
        }
        $this->load->view('css_fly/woo/charity/woo_charity_default');

    } 
    public function css_woo()
    {

        if (array_key_exists("HTTP_REFERER", $_SERVER)) {

            if (false !== stripos($_SERVER['HTTP_REFERER'], "dufner-shop.de")) {
                $this->load->view('css_fly/woo/charity/dufner-shop');
                return;
            }
    
        }
        $this->load->view('css_fly/woo/woo_default');

    }

/* backup the db OR just a table */
    public function backup_tables($host, $user, $pass, $name, $tables = '*')
    {

        $link = mysqli_connect($host, $user, $pass, $name);
        // mysqli_select_db($name, $link);

        //get all of the tables
        if ($tables == '*') {
            $tables = array();
            $result = mysqli_query($link, 'SHOW TABLES');
            while ($row = mysqli_fetch_row($result)) {
                $tables[] = $row[0];
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        // print_r($tables);

        $backup_return = '';

        //cycle through
        foreach ($tables as $table) {

            $result     = mysqli_query($link, 'SELECT * FROM ' . $table);
            $num_fields = mysqli_num_fields($result);

            $backup_return .= 'DROP TABLE ' . $table . ';';
            $row2 = mysqli_fetch_row(mysqli_query($link, 'SHOW CREATE TABLE ' . $table));
            $backup_return .= "\n\n" . $row2[1] . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysqli_fetch_row($result)) {
                    $backup_return .= 'INSERT INTO ' . $table . ' VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = str_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {$backup_return .= '"' . $row[$j] . '"';} else { $backup_return .= '""';}
                        if ($j < ($num_fields - 1)) {$backup_return .= ',';}
                    }
                    $backup_return .= ");\n";
                }
            }
            $backup_return .= "\n\n\n";
        }

        //save file
        $path = $_SERVER['DOCUMENT_ROOT'] . '/dbs';
        if (!is_dir($path)) {
            mkdir($path, 0750, true);
        }
        $path .= "/";
        $handle = fopen($path . $name . '-' . date("Y-m-d") . '-' . (md5(implode(',', $tables))) . '.sql', 'w+');
        fwrite($handle, $backup_return);
        fclose($handle);
    }

    public function do_do_do()
    {
        $dbhost = 'localhost';
        $dbuser = 'philip_cronback';
        $dbpass = 'qbRxxl2~4b.h';
        $dbname = 'philip_portal';

        $dbuser = 'wp_user';
        $dbpass = 'njinji';
        $dbname = 'womby_client_portal';
        // $mysqldump = exec('which mysqldump');

        $this->backup_tables($dbhost, $dbuser, $dbpass, $dbname);
        // $this->backup_tables($dbhost, $dbuser, $dbpass, "philip_collector");

    }

    public function woocommerce()
    {
        if (!is_logged()) {
            $this->session->set_userdata('last_page', current_url());
            redirect_login();
        }
        //remove payment to another page
        // if (!has_confirmed()) {
        //     redirect_confirm();
        // }

        $this->load->view('dashboard/woocommerce');
    }
    public function magento()
    {
        if (!is_logged()) {
            $this->session->set_userdata('last_page', current_url());

            redirect_login();
        }
        //remove payment to another page
        // if (!has_confirmed()) {
        //     redirect_confirm();
        // }

        $this->load->view('dashboard/magento');
    }
    public function shopware()
    {
        if (!is_logged()) {
            $this->session->set_userdata('last_page', current_url());

            redirect_login();
        }

        //remove payment to another page
        // if (!has_confirmed()) {
        //     redirect_confirm();
        // }

        $this->load->view('dashboard/shopware');
    }

    public function choose()
    {
        if (!is_logged()) {
            $this->session->set_userdata('last_page', current_url());

            redirect_login();
        }

        //remove payment to another page
        if (!has_confirmed()) {
            redirect_confirm();
        }
        $this->load->view('dashboard/choose');
    }

    public function click()
    {
        if (!is_logged()) {
            $this->session->set_userdata('last_page', current_url());

            redirect_login();
        }

        //remove payment to another page
        if (!has_confirmed()) {
            redirect_confirm();
        }
       
        $this->load->model('clickmodel');
        $DBCC = $this->load->database('charity_collector',TRUE);
        $data['testing'] = $this->clickmodel->getPosts($DBCC);
        $this->load->view('dashboard/click', $data);
    }

    public function charity($edit = false)
    {
        if (!is_logged()) {
            $this->session->set_userdata('last_page', current_url());

            redirect_login();
        }
        //remove payment to another page
        // if (!has_confirmed()) {
        //     redirect_confirm();
        // }
        if (!$edit) {
            $data         = $this->session->userdata('signup_data_2');
            $data['edit'] = false;
        } else {

            $user_id = $this->session->userdata('user_id');

            $user_details = $this->users_model->get($user_id);

            $company_details = $this->companies_model->get_by(array('user_id' => $user_id));

            $payment_details = $this->payments_model->get_by(array('user_id' => $user_id));

            //append token & customer ID
            $payment_details['token']   = $user_details['token'];
            $payment_details['user_id'] = $user_id;

            $data = array_merge($company_details, $payment_details);

        }
        $data['charity_refrence'] = "ch" . generate_token();

        $data['charities'] = $this->charities_model->get_all();

        $this->load->view('user/signup_charity', $data);
    }

    public function do_charity()
    {
        if (!is_logged()) {
            redirect_login();
        }
        //remove payment to another page
        if (!has_confirmed()) {
            redirect_confirm();
        }

        $this->form_validation->set_rules('confirmation_1', 'confirmation_1', 'required');
        $this->form_validation->set_rules('confirmation_2', 'confirmation_2', 'required');
        if ($this->form_validation->run() == false) {
            // validation_errors();
            redirect('dashboard/charity/');
        }
        $user_id = $this->session->userdata('user_id');

        $this->users_model->update($user_id, array(
            'charity_token'    => $this->input->post('charity_refrence'),
            'filled_charity'   => 1,
            'selected_charity' => $this->input->post('charity'),
        ));

        $this->session->set_flashdata('warnings', $this->lang->line("dashboard_filled"));

        $webs = $this->CompanyWebsites_model->get_many_by(array('user_id' => $user_id));

        foreach ($webs as $web) {

            $this->CompanyWebsites_model->update($web['id'], array(
                'selected_charity' => $this->input->post('charity'),
            ));

        }

        redirect_home();

    }

}
