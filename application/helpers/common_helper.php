<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * @author XtarX
 * @author ahmedmahmoudit
 * @package helper.common
 */

if (!function_exists('sendmail')) {

    function sendmail($to, $subject, $message)
    {
        $CI = get_instance();

        $CI->load->library('email');
        $CI->email->set_mailtype("html");

        $CI->email->from(FROM_MAIL, FROM_NAME);
        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($message);
        $CI->email->send();

    }
}

if (!function_exists('generate_token')) {
    /**
     * This method is used to Generate Token.
     * @method generate_token
     * @return {String} Generated token.
     *
     */
    function generate_token()
    {
        // $token = md5(uniqid() . microtime() . rand());
        // return $token;
        return uniqid();
    }
}

if (!function_exists('redirect_home')) {

    function redirect_home()
    {

        redirect('dashboard/choose');

    }
}if (!function_exists('redirect_edit')) {

    function redirect_edit()
    {

        redirect('user/edit');

    }
}

if (!function_exists('redirect_login')) {

    function redirect_login()
    {

        redirect('user/login');

    }
}

if (!function_exists('redirect_confirm')) {

    function redirect_confirm()
    {

        redirect('user/edit');

    }
}

if (!function_exists('is_logged')) {

    function is_logged()
    {
        $CI = get_instance();

        $session_user = $CI->session->userdata('user_id');

        if (!isset($session_user) || ($session_user == '')) {
            return false;
        }
        return true;
    }
}

if (!function_exists('has_confirmed')) {

    function has_confirmed()
    {
        if (!is_logged()) {
            return false;
        }
        $CI = get_instance();

        $session_user = $CI->session->userdata('user_id');

        return $CI->users_model->get_by(array('id' => $session_user, 'filled' => 1));

    }
}

if (!function_exists('has_filled_charity')) {

    function has_filled_charity()
    {
        if (!is_logged()) {
            return false;
        }
        $CI = get_instance();

        $session_user = $CI->session->userdata('user_id');

        return $CI->users_model->get_by(array('id' => $session_user, 'filled_charity' => 1));

    }
}

if (!function_exists('whatever')) {

    function whatever($array, $key, $val)
    {
        foreach ($array as $item) {
            if (isset($item[$key]) && $item[$key] == $val) {
                return true;
            }
        }

        return false;
    }

}
