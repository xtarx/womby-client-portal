<?php
defined('BASEPATH') or exit('No direct script access allowed');
//gcloud app deploy --project even-electron-151909
//gcloud beta sql connect womby-portal --user=root
$active_group  = 'default';
$query_builder = true;

$db['default'] = array(
    'dsn'          => '',
    'hostname'     => '/cloudsql/even-electron-151909:europe-west1:womby-portal',
    'username'     => 'womby_priv',
    'password'     => 'womby',
    'database'     => 'portal',
    // 'hostname'     => '104.199.9.118',

    // 'hostname'     => 'localhost',
    // 'username'     => 'wp_user',
    // 'password'     => 'njinji',
    // 'database'     => 'womby_client_portal',

    'dbdriver'     => 'mysqli',
    'dbprefix'     => '',
    'pconnect'     => false,
    'db_debug'     => (ENVIRONMENT !== 'production'),
    'cache_on'     => false,
    'cachedir'     => APPPATH . 'cache',
    'char_set'     => 'utf8',
    'dbcollat'     => 'utf8_general_ci',
    'swap_pre'     => '',
    'encrypt'      => false,
    'compress'     => false,
    'stricton'     => false,
    'failover'     => array(),
    'save_queries' => true,
);

$db['charity_collector'] = array(
    'dsn'          => '',
    'hostname'     => '/cloudsql/even-electron-151909:europe-west1:womby-portal',
    'username'     => 'womby_priv',
    'password'     => 'womby',
    'database'     => 'charity_collector',
    // 'hostname'     => '104.199.9.118',

    // 'hostname'     => 'localhost',
    // 'username'     => 'wp_user',
    // 'password'     => 'njinji',
    // 'database'     => 'womby_client_portal',

    'dbdriver'     => 'mysqli',
    'dbprefix'     => '',
    'pconnect'     => false,
    'db_debug'     => (ENVIRONMENT !== 'production'),
    'cache_on'     => false,
    'cachedir'     => APPPATH . 'cache',
    'char_set'     => 'utf8',
    'dbcollat'     => 'utf8_general_ci',
    'swap_pre'     => '',
    'encrypt'      => false,
    'compress'     => false,
    'stricton'     => false,
    'failover'     => array(),
    'save_queries' => true,
);