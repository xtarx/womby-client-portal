<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Companies_model extends MY_Model
{

    protected $_table      = 'companies';
    protected $return_type = 'array';

}
