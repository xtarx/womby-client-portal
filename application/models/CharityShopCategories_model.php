<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CharityShopCategories_model extends MY_Model
{

    protected $_table      = 'charity_shop_categories';
    protected $return_type = 'array';

}