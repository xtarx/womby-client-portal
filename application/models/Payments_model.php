<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Payments_model extends MY_Model
{

    protected $_table      = 'payments';
    protected $return_type = 'array';

}
