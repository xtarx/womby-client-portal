<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CharityWebsiteCategories_model extends MY_Model
{

    protected $_table      = 'charity_website_categories';
    protected $return_type = 'array';

}