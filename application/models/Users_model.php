<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class users_model extends MY_Model
{

    protected $_table      = 'users';
    protected $return_type = 'array';

    public function send_verification($data)
    {

        $message = $this->load->view('email/verify', $data, true);

        sendmail($data['email'], "verify your account", $message);
    }

    public function send_text($data)
    {

        $message = $this->load->view('email/general_text', $data, true);

        sendmail($data['email'], $data['title'], $message);
    }

/**
 * Update password reset sent datetime
 *
 * @access public
 * @param int $account_id
 * @return int password reset time
 */
    public function update_reset_sent_datetime($account_id)
    {
        $this->load->helper('date');

        $resetsenton = mdate('%Y-%m-%d %H:%i:%s', now());

        $this->db->update('users', array('resetsenton' => $resetsenton), array('id' => $account_id));

        return strtotime($resetsenton);
    }

    /**
     * Remove password reset datetime
     *
     * @access public
     * @param int $account_id
     * @return void
     */
    public function remove_reset_sent_datetime($account_id)
    {
        $this->db->update('users', array('resetsenton' => null), array('id' => $account_id));
    }

    public function update_last_signed_in_datetime($account_id)
    {
        $this->load->helper('date');

        $this->db->update('users', array('lastsignedinon' => mdate('%Y-%m-%d %H:%i:%s', now())), array('id' => $account_id));
    }

}
