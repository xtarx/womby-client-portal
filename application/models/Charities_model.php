<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Charities_model extends MY_Model
{

    protected $_table      = 'charities';
    protected $return_type = 'array';

}
