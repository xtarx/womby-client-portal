<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Countries_model extends MY_Model
{

    protected $_table      = 'countries';
    protected $return_type = 'array';

}
