<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Installs_info_model extends MY_Model
{

    protected $_table      = 'installs_info';
    protected $return_type = 'array';

}
