<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class UserEdits_model extends MY_Model
{

    protected $_table      = 'user_edits';
    protected $return_type = 'array';

}