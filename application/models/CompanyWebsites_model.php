<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class CompanyWebsites_model extends MY_Model
{

    protected $_table      = 'company_websites';
    protected $return_type = 'array';

    public function hash($url)
    {

        if (strpos($url, "://") === false && substr($url, 0, 1) != "/") {
            $url = "http://" . $url;
        }

        $url        = parse_url($url, PHP_URL_HOST);
        $host_names = explode(".", $url);

        return md5($this->trimWord($url));
    }

    public function is_subdomain($url)
    {

        if (strpos($url, "://") === false && substr($url, 0, 1) != "/") {
            $url = "http://" . $url;
        }

        $url = parse_url($url, PHP_URL_HOST);

        $host_names = explode(".", $url);

        return sizeof($host_names) > 2;
    }
    public function hash_subdomain($url)
    {

        if (strpos($url, "://") === false && substr($url, 0, 1) != "/") {
            $url = "http://" . $url;
        }

        $url = parse_url($url, PHP_URL_HOST);

        $host_names       = explode(".", $url);
        $bottom_host_name = $host_names[count($host_names) - 2] . "." . $host_names[count($host_names) - 1];

        return md5($this->trimWord($bottom_host_name));
    }

    public function compareUrls($a, $b)
    {
        $a = parse_url($a, PHP_URL_HOST);
        $b = parse_url($b, PHP_URL_HOST);

        return $this->trimWord($a) === $this->trimWord($b);
    }

    public function trimWord($str)
    {
        if (stripos($str, 'www.') === 0) {
            return substr($str, 4);
        }
        return $str;
    }

}
